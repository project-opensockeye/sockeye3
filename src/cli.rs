use crate::decoding_net;
use crate::decoding_net::{AddressSpace, AddressSpaceAttribute, ResourceType};
use crate::glas::{self, Glas, GlasDerivationOptions};
use crate::output::human_bytes;
use crate::range::{Range, RangeSet};
use crate::specification::Specification;

use std::collections::{BTreeMap, BTreeSet};
use std::fs::File;
use std::io::{self, Write};
use std::path::{Path, PathBuf};
use std::time::Duration;

use clap::Parser;
use env_logger::{Builder, Env};
use indicatif::*;
use indicatif_log_bridge::LogWrapper;
use log::*;
use miette::{miette, IntoDiagnostic, Result};

use serde::Serialize;

#[derive(Parser, Debug)]
#[command(about, version)]
struct Args {
    #[arg(required = true, value_name = "SPEC_PATH")]
    specification_path: PathBuf,

    #[arg(
        requires = "glas_out",
        long,
        help = "Derive a global logical address space"
    )]
    derive_glas: bool,

    #[arg(
        long,
        conflicts_with = "derive_glas",
        help = "Path to deserialize the GLAS from"
    )]
    load_glas: Option<PathBuf>,

    #[arg(
        long,
        requires = "derive_glas",
        value_name = "FILE",
        help = "Path to serialize the GLAS to"
    )]
    save_glas: Option<PathBuf>,

    #[arg(
        long,
        requires = "derive_glas",
        value_name = "FILE",
        help = "Path to output GLAS to"
    )]
    glas_out: Option<PathBuf>,

    #[arg(
        long,
        requires = "derive_glas",
        value_name = "FILE",
        help = "Additional constraints for GLAS derivation"
    )]
    glas_constraints: Option<PathBuf>,

    #[arg(
        long,
        requires = "derive_glas",
        help = "Attempt to derive an optimal GLAS, minimizing the number of non-zero offsets.",
        default_value_t = false
    )]
    glas_try_optimal: bool,

    #[arg(long, requires = "glas_out", value_parser = ["json"])]
    glas_format: Option<String>,

    #[arg(long, help = "Path to output the hardware matrix to")]
    matrix_out: Option<PathBuf>,

    #[arg(long, requires = "matrix_out", default_value = "text", value_parser = ["json", "text"])]
    matrix_format: String,

    #[arg(
        long,
        value_name = "FILE",
        help = "File path to output a graphviz visualization to"
    )]
    graphviz: Option<PathBuf>,

    #[arg(
        long,
        value_name = "ADDRESS_SPACE",
        help = "Address space to focus (e.g. for visualizations)"
    )]
    focus: Option<String>,

    #[arg(short, long, help = "Enable verbose output")]
    verbose: bool,
}

struct State<'a> {
    bar: &'a ProgressBar,
    args: Args,
    spec: Specification,
    glas: Option<Glas>,
    glas_options: GlasDerivationOptions,
}

pub fn run() -> Result<()> {
    let args = Args::parse();

    let bar = ProgressBar::new_spinner()
        .with_style(
            ProgressStyle::with_template("{spinner} {msg} [{elapsed}]")
                .unwrap(),
        )
        .with_finish(ProgressFinish::AndClear);
    bar.enable_steady_tick(Duration::from_millis(100));
    let multi = MultiProgress::new();
    let bar = multi.add(bar);

    let logger = Builder::from_env(Env::default().default_filter_or("info"))
        .format_timestamp(None)
        .build();
    LogWrapper::new(multi, logger).try_init().unwrap();

    bar.set_message(format!(
        "Parsing specification {:?}...",
        args.specification_path
    ));
    let spec = Specification::parse(&args.specification_path)?;
    info!(
        "Parsed specification from {} ({} address spaces) [{:#}]",
        args.specification_path
            .file_name()
            .unwrap()
            .to_string_lossy(),
        spec.decoding_net.address_spaces.len(),
        HumanDuration(bar.elapsed())
    );

    let mut state = State {
        bar: &bar,
        args,
        spec,
        glas: None,
        glas_options: GlasDerivationOptions {
            constraints: Vec::new(),
            try_optimal: false,
        },
    };
    state.run()?;
    info!(
        "Successful run, terminating [{:#}]",
        HumanDuration(bar.elapsed())
    );

    Ok(())
}

impl<'a> State<'a> {
    pub fn run(&mut self) -> Result<()> {
        if let Some(path) = &self.args.glas_constraints {
            self.bar.set_message(format!(
                "Parsing GLAS constraints from {:?}...",
                path,
            ));
            self.glas_options.constraints =
                GlasDerivationOptions::parse_constraints(
                    path,
                    &self.spec.decoding_net,
                )?;
            info!(
                "Parsed GLAS constraints [{:#}]",
                HumanDuration(self.bar.elapsed())
            );
        }

        if self.args.derive_glas {
            self.bar.set_message("Deriving GLAS...");
            self.glas = Some(crate::glas::derive_glas(
                &self.spec.decoding_net,
                &self.glas_options,
            )?);
            info!("Derived GLAS [{:#}]", HumanDuration(self.bar.elapsed()));
        } else if let Some(path) = &self.args.load_glas {
            self.bar.set_message("Loading GLAS...");
            let f = File::open(path).map_err(|err| {
                miette!(code = "cli::load_glas", "cannot read GLAS: {}", err)
            })?;
            self.glas = Some(serde_json::from_reader(f).into_diagnostic()?);
            info!(
                "Loaded GLAS from file {path:?} [{:#}]",
                HumanDuration(self.bar.elapsed())
            );
        }

        if let Some(path) = &self.args.save_glas {
            self.bar.set_message("Serializing GLAS...");
            let f = File::create(path).map_err(|err| {
                miette!(code = "cli::save_glas", "cannot write file: {}", err)
            })?;
            serde_json::to_writer(f, &self.glas.as_ref().unwrap())
                .into_diagnostic()?;
            info!("Serialized GLAS to {path:?}");
        }

        if let Some(path) = &self.args.glas_out {
            self.bar.set_message("Writing GLAS...");
            let mut f = File::create(path).map_err(|err| {
                miette!(code = "cli::glas_out", "cannot write file: {}", err)
            })?;
            glas::write_json(
                &mut f,
                &self.spec.decoding_net,
                self.glas.as_ref().unwrap(),
            )
            .map_err(|err| {
                miette!(code = "cli::glas_out", "cannot write file: {}", err)
            })?;
            info!("Wrote GLAS to {path:?}");
        }

        if let Some(path) = &self.args.matrix_out {
            self.bar.set_message("Writing hardware matrix...");
            output_hardware_matrix(&self.spec, path, &self.args.matrix_format)?;
            info!(
                "Wrote hardware matrix as {} to {path:?}",
                self.args.matrix_format
            );
        }

        match &self.args.focus {
            Some(name) => self.f_focused(name)?,
            None => self.f_unfocused()?,
        }

        Ok(())
    }

    fn f_unfocused(&self) -> Result<()> {
        if let Some(path) = &self.args.graphviz {
            self.bar
                .set_message(format!("Outputting dot drawing to {path:?}..."));
            output_graphviz(path, &self.spec).into_diagnostic()?;
            info!("Dot output to {path:?}");
        }

        Ok(())
    }

    fn f_focused(&self, name: &str) -> Result<()> {
        self.bar
            .set_message(format!("Focusing address space {name}... "));
        let a = self.spec.decoding_net.ensure_address_space_exists(name)?;

        print_info(a);

        Ok(())
    }
}

fn output_graphviz(path: &Path, spec: &Specification) -> io::Result<()> {
    let mut f = std::fs::File::create(path)?;
    writeln!(f, "digraph {{")?;
    writeln!(f, "  rankdir=\"LR\";")?;
    writeln!(f, "  node [shape=record,style=filled];")?;
    for (name, instance) in spec.instances.iter() {
        writeln!(f, "  subgraph cluster_{name} {{")?;
        writeln!(
            f,
            "    label = \"module {}\n{name}\";",
            instance.module_name
        )?;
        for addr_space in instance.decoding_net.address_spaces.values() {
            writeln!(
                f,
                "    \"{name}.{}\" [label = \"{}\"];",
                addr_space.name, addr_space.name
            )?;
        }
        writeln!(f, "  }}")?;
    }

    for (name, addr_space) in spec.decoding_net.address_spaces.iter() {
        let color = match addr_space.attribute {
            decoding_net::AddressSpaceAttribute::None => "lightblue",
            decoding_net::AddressSpaceAttribute::Context => "orange",
            decoding_net::AddressSpaceAttribute::Resource(_) => "black",
        };
        let fontcolor = match addr_space.attribute {
            decoding_net::AddressSpaceAttribute::None => "black",
            decoding_net::AddressSpaceAttribute::Context => "black",
            decoding_net::AddressSpaceAttribute::Resource(_) => "white",
        };
        writeln!(
            f,
            "  \"{}\" [color=\"{color}\",fontcolor=\"{fontcolor}\"]",
            name
        )?;
    }

    for (name, addr_space) in spec.decoding_net.address_spaces.iter() {
        for entry in addr_space.mappings.iter_mapped() {
            let col = if name.split(".").next() == entry.dst.split(".").next() {
                "#888888"
            } else {
                "black"
            };
            writeln!(
                f,
                "  \"{}\" -> \"{}\" [color=\"{col}\"]",
                name, entry.dst
            )?;
        }
    }

    writeln!(f, "}}")
}

fn print_info(a: &AddressSpace) {
    let ty = match a.attribute {
        AddressSpaceAttribute::None => "address_space",
        AddressSpaceAttribute::Context => "context",
        AddressSpaceAttribute::Resource(ResourceType::Device) => "device",
        AddressSpaceAttribute::Resource(ResourceType::Memory) => "memory",
        AddressSpaceAttribute::Resource(ResourceType::Unknown) => "resource",
    };

    println!("{ty:20} {}", a.name);
    for line in textwrap::wrap(&a.description, textwrap::Options::new(78)) {
        println!("  {line}");
    }

    let r = a.range;
    println!("valid addresses {:#x} - {:#x}", r.start(), r.last(),);

    println!("size {}", human_bytes(a.range.length()),);

    let num_mapped_ranges = a.mappings.iter_mapped().count();
    if num_mapped_ranges == 0 {
        println!("no mappings");
    } else {
        println!("mappings");
        for (src_range, ms) in a.mappings.iter() {
            for m in ms {
                println!(
                    "{:#20x} - {:#20x} maps to {:20} @ {:#20x}   {}",
                    src_range.start(),
                    src_range.last(),
                    m.dst,
                    m.dst_range.offset(),
                    m.description,
                );
            }
        }
    }
}

#[derive(Serialize)]
struct HardwareMatrix {
    contexts: Vec<String>,
    resource_ranges: Vec<(String, Range)>,
    can_access: Vec<Vec<bool>>,
}

fn output_hardware_matrix(
    spec: &Specification,
    path: &Path,
    format: &str,
) -> Result<()> {
    let matrix = compute_hardware_matrix(spec)?;
    let f = File::create(path).into_diagnostic()?;
    match format {
        "json" => write_hardware_matrix_as_json(&matrix, f),
        "text" => write_hardware_matrix_as_text(&matrix, f),
        _ => unreachable!(),
    }
}

fn compute_hardware_matrix(spec: &Specification) -> Result<HardwareMatrix> {
    let contexts: Vec<String> = spec
        .decoding_net
        .address_spaces
        .iter()
        .filter(|(_, a)| a.is_context())
        .map(|(_, a)| a.name.clone())
        .collect();

    let resources: BTreeSet<String> = spec
        .decoding_net
        .address_spaces
        .iter()
        .filter(|(_, a)| a.is_resource())
        .map(|(_, a)| a.name.clone())
        .collect();

    let mut resource_ranges = BTreeMap::new();
    for res in resources.iter() {
        resource_ranges.insert(
            res.to_string(),
            RangeSet::new(spec.decoding_net.address_spaces[res].range),
        );
    }

    let flattened = spec.decoding_net.flatten_contexts()?;
    for (_, space) in flattened.iter() {
        for (dst, dst_range) in space.iter_mapped() {
            if !resources.contains(dst) {
                continue;
            }

            resource_ranges
                .get_mut(dst)
                .unwrap()
                .split_at(dst_range.start());
            resource_ranges
                .get_mut(dst)
                .unwrap()
                .split_at(dst_range.end());
        }
    }

    let resource_ranges: Vec<(String, Range)> = resource_ranges
        .iter()
        .flat_map(|(k, rs)| rs.iter().map(|(r, _)| (k.to_string(), r)))
        .collect();
    let mut can_access = Vec::new();
    for ctx in contexts.iter() {
        let mut v = vec![false; resource_ranges.len()];

        let space = &flattened[ctx];
        for (dst, dst_range) in space.iter_mapped() {
            if !resources.contains(dst) {
                continue;
            }

            for (i, (_, r)) in resource_ranges
                .iter()
                .enumerate()
                .filter(|(_, (d, _))| d == dst)
            {
                if dst_range.contains(r) {
                    v[i] = true;
                }
            }
        }
        can_access.push(v);
    }

    Ok(HardwareMatrix {
        contexts,
        resource_ranges,
        can_access,
    })
}

fn write_hardware_matrix_as_json(
    matrix: &HardwareMatrix,
    f: impl io::Write,
) -> Result<()> {
    serde_json::to_writer(f, matrix).into_diagnostic()?;
    Ok(())
}

fn write_hardware_matrix_as_text(
    matrix: &HardwareMatrix,
    mut f: impl io::Write,
) -> Result<()> {
    writeln!(f, "hardware protection matrix").into_diagnostic()?;
    for _ in matrix.resource_ranges.iter() {
        write!(f, "  ").into_diagnostic()?;
    }
    writeln!(f, "  contexts").into_diagnostic()?;

    for _ in matrix.resource_ranges.iter() {
        write!(f, "  ").into_diagnostic()?;
    }
    writeln!(f, "  --------").into_diagnostic()?;

    for i in 0..matrix.contexts.len() {
        for j in 0..matrix.resource_ranges.len() {
            if matrix.can_access[i][j] {
                write!(f, "x ").into_diagnostic()?;
            } else {
                write!(f, "  ").into_diagnostic()?;
            }
        }

        writeln!(f, "| {}", matrix.contexts[i]).into_diagnostic()?;
    }

    for _ in 0..matrix.resource_ranges.len() {
        write!(f, "--").into_diagnostic()?;
    }
    writeln!(f, "+").into_diagnostic()?;

    let mut i = 0;
    let mut do_continue = true;
    while do_continue {
        do_continue = false;
        for (r, ro) in matrix.resource_ranges.iter() {
            let s = format!("{r} @ {:#x}..{:#x}", ro.start(), ro.end());
            if i < s.len() {
                do_continue = true;
                write!(f, "{} ", s.as_bytes()[i] as char).into_diagnostic()?;
            } else {
                write!(f, "  ").into_diagnostic()?;
            }
        }

        write!(f, "| ").into_diagnostic()?;

        if i < "resources".len() {
            do_continue = true;
            write!(f, "{} ", "resources".as_bytes()[i] as char)
                .into_diagnostic()?;
        } else {
            write!(f, "  ").into_diagnostic()?;
        }

        i += 1;
        writeln!(f).into_diagnostic()?;
    }

    Ok(())
}

//#[rustfmt::skip]
//const CMDS: BTreeMap<&str, (&str, &str)> = BTreeMap::from([
//    ("add_glas_constraint", ("name phys_addr glas_addr len",
//                             "Add a constraint to the GLAS to force (phys_addr, len) in name to map to (glas_addr, len)")),
//    ("as_info",             ("name", "Print info about given address_space")),
//    ("exit",                ("", "Exit the REPL")),
//    ("derive_glas",         ("", "Derive the GLAS")),
//    ("flattened_context",   ("name", "Print details of flattened context")),
//    ("gen_pt",              ("name base_addr path", "Generate page tables as C header file for the given context")),
//    ("glas",                ("", "Print the GLAS")),
//    ("swizzles",            ("[name]", "Print the required swizzles")),
//    ("glas_for",            ("name", "Print the GLAS for a specific core")),
//    ("glas_json",           ("path", "Write the GLAS description as JSON to path")),
//    ("hw_matrix",           ("[path]", "Output the hardware protection matrix")),
//    ("help",                ("", "Print available commands")),
//    ("info",                ("", "Print info about the current model")),
//    ("load_glas",           ("path", "Load a derived GLAS from a file")),
//    ("mark_context",        ("name", "Mark an address space as a context")),
//    ("mark_resource",       ("name", "Mark an address space as a resource")),
//    ("models",              ("", "List available models")),
//    ("quit",                ("", "Quit the REPL")),
//    ("read_file",           ("path", "Read and execute the commands in from a file")),
//    ("save_glas",           ("path", "Save the derived GLAS to a file")),
//    ("set_model",           ("name", "Set the current model")),
//    ("trace_path",          ("name addr", "Trace address decoding path of given address")),
//    ("viz_dn",              ("path [open]", "Visualize the decoding net using graphviz")),
//    ("viz_glas",            ("path [open]", "Visualize the GLAS using graphviz")),
//    ("viz_glas_for",        ("name path [open]", "Visualize the GLAS for a specific core using graphviz")),
//]);

//    fn exec_line<'a>(
//        self: &mut State,
//        line: &'a str,
//    ) -> (Option<&'a str>, Result<(), ReplError>) {
//        let mut args = line.split_whitespace();
//        let cmd = args.next();
//        let res = match cmd {
//            Some("add_glas_constraint") => self.add_glas_constraint(&mut args),
//            Some("as_info") => self.print_address_space_info(&mut args),
//            Some("derive_glas") => self.derive_glas(),
//            Some("gen_pt") => self.generate_page_table_for_context(&mut args),
//            Some("glas") => self.print_glas(),
//            Some("swizzles") => self.print_swizzles(&mut args),
//            Some("glas_for") => self.print_glas_from_view(&mut args),
//            Some("glas_json") => self.glas_json(&mut args),
//            Some("hw_matrix") => self.print_hardware_matrix(&mut args),
//            Some("flattened_context") => {
//                self.print_flattened_context(&mut args)
//            }
//            Some("info") => self.print_info(),
//            Some("load_glas") => self.load_glas(&mut args),
//            Some("models") => self.list_models(),
//            Some("save_glas") => self.save_glas(&mut args),
//            Some("set_model") => self.set_model(&mut args),
//            Some("read_file") => self.read_file(&mut args),
//            Some("trace_path") => self.trace_path(&mut args),
//            Some("viz_dn") => self.visualize_dn(&mut args),
//            Some("viz_glas") => self.visualize_glas(&mut args),
//            Some("viz_glas_for") => self.visualize_glas_for(&mut args),
//            Some("quit") => Err(ReplError::Quit),
//            Some("exit") => Err(ReplError::Quit),
//            Some("help") => Err(ReplError::Help),
//            Some(unknown) => {
//                Err(ReplError::UnknownCommand(unknown.to_string()))
//            }
//            None => Err(ReplError::NoArgumentProvided),
//        };
//
//        (cmd, res)
//    }
//
//
//
//    fn print_flattened_context<'a>(
//        &self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        let (name, asid) = Self::next_asid(&self.model, args)?;
//        self.model
//            .contexts
//            .contains(&asid)
//            .then_some(asid)
//            .ok_or(ReplError::NotAContext(name.to_string()))?;
//
//        let spec = glas::DecodingNet::from_spec(&self.model).unwrap();
//        let f = spec.flatten(&spec.contexts);
//        println!("flattened context {}", name);
//
//        for &(src_range, r, dst_range) in f.r[&asid.as_usize()].iter() {
//            let r_name = &self.model.address_spaces[r].name;
//            println!(
//                "{:#20x} - {:#20x} maps to {:20} @ {:#20x}",
//                src_range.offset,
//                src_range.offset + src_range.length - 1,
//                r_name,
//                dst_range.offset
//            );
//        }
//
//        Ok(())
//    }
//
//    fn trace_path<'a>(
//        &self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        let (name, asid) = Self::next_asid(&self.model, args)?;
//        let addr = Self::next_num(args)?;
//
//        println!(
//            "tracing decoding of address {:#x} in address space {}",
//            addr, name
//        );
//
//        let mut a = asid;
//        let mut addr = addr;
//        loop {
//            let space = &self.model.address_spaces[a.as_usize()];
//            if addr < space.range.offset
//                || addr >= space.range.offset + space.range.length
//            {
//                return Err(ReplError::AddressOutOfRange(addr, space.range));
//            }
//
//            let current_mappings = &self.model.mappings[a.as_usize()];
//            if current_mappings.len() == 0 {
//                break;
//            }
//
//            let idx = current_mappings.binary_search_by_key(&addr, |m| {
//                m.src_range.offset + m.src_range.length - 1
//            });
//
//            let idx = match idx {
//                Ok(idx) => idx,
//                Err(idx) => idx,
//            };
//
//            assert!(idx < current_mappings.len());
//            let mapping = &current_mappings[idx];
//            println!(
//                "{:#20x} in {:10}   -- {}",
//                addr, space.name, mapping.description
//            );
//
//            if mapping.src_range.offset <= addr
//                && addr < mapping.src_range.offset + mapping.src_range.length
//            {
//                let diff = mapping.dst_range.offset as i128
//                    - mapping.src_range.offset as i128;
//                addr = (addr as i128 + diff) as u128;
//                a = mapping.dst;
//            } else {
//                break;
//            }
//        }
//
//        println!(
//            "{:#20x} in {:10}",
//            addr,
//            self.model.address_spaces[a.as_usize()].name
//        );
//        Ok(())
//    }
//
//    fn print_info(&self) -> Result<(), ReplError> {
//        println!("selected model {}", self.chosen);
//        println!(
//            "number of address spaces: {}",
//            self.model.address_spaces.len()
//        );
//        print!("contexts ({}): ", self.model.contexts.len());
//        println!(
//            "{}",
//            self.model
//                .contexts
//                .iter()
//                .map(|c| self.model.address_spaces[c.as_usize()].name.clone())
//                .collect::<Vec<String>>()
//                .join(", ")
//        );
//        print!("resources ({}): ", self.model.resources.len());
//        println!(
//            "{}",
//            self.model
//                .resources
//                .iter()
//                .map(|r| self.model.address_spaces[r.as_usize()].name.clone())
//                .collect::<Vec<String>>()
//                .join(", ")
//        );
//        match &self.glas {
//            None => println!("glas: not derived"),
//            Some(glas) => println!(
//                "glas: derived ({} resources)",
//                glas.1
//                    .placement
//                    .iter()
//                    .map(|b| b.0)
//                    .collect::<BTreeSet<_>>()
//                    .len()
//            ),
//        }
//
//        Ok(())
//    }
//
//    fn derive_glas(&mut self) -> Result<(), ReplError> {
//        let spec = glas::DecodingNet::from_spec(&self.model).unwrap();
//        let glas = glas::derive_glas(
//            &spec,
//            &spec.contexts,
//            &spec.resources,
//            &self.glas_constraints,
//        )?;
//        self.glas = Some((spec, glas));
//        Ok(())
//    }
//
//    fn print_glas(&mut self) -> Result<(), ReplError> {
//        match &self.glas {
//            None => self.derive_glas()?,
//            _ => (),
//        }
//
//        let (spec, glas) = self.glas.as_ref().unwrap();
//        glas::print_glas(glas, spec);
//
//        Ok(())
//    }
//
//    fn print_swizzles<'a>(
//        &mut self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        match &self.glas {
//            None => self.derive_glas()?,
//            _ => (),
//        }
//        let (spec, glas) = self.glas.as_ref().unwrap();
//
//        match Self::next_asid(&self.model, args) {
//            Ok((name, asid)) => {
//                let asid = asid.as_usize();
//                let _ = spec
//                    .contexts
//                    .contains(&asid)
//                    .then_some(asid)
//                    .ok_or(ReplError::NotAContext(name.to_string()))?;
//                glas::print_swizzles_for(&glas, &spec, asid);
//            }
//            Err(ReplError::NoArgumentProvided) => {
//                glas::print_swizzles(&glas, &spec)
//            }
//            Err(e) => return Err(e),
//        };
//
//        Ok(())
//    }
//
//    fn print_glas_from_view<'a>(
//        &mut self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        match self.glas {
//            None => self.derive_glas()?,
//            _ => (),
//        }
//        let (spec, _glas) = self.glas.as_ref().unwrap();
//
//        let (name, asid) = Self::next_asid(&self.model, args)?;
//        let asid = asid.as_usize();
//        let _ = spec
//            .contexts
//            .contains(&asid)
//            .then_some(asid)
//            .ok_or(ReplError::NotAContext(name.to_string()))?;
//
//        Ok(())
//    }
//
//    fn glas_json<'a>(
//        &mut self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        match &self.glas {
//            None => self.derive_glas()?,
//            _ => (),
//        }
//        let (spec, glas) = self.glas.as_ref().unwrap();
//        let f = spec.flatten(&spec.contexts);
//
//        let path = Self::next_arg(args)?;
//        let mut file = std::fs::File::create(path)?;
//        output::glas::json(&mut file, &glas, &spec, &f, &spec.resources)?;
//        println!("wrote GLAS json to '{}'", path);
//
//        Ok(())
//    }
//
//    fn open_if_desired<'a>(
//        path: &str,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        match Self::next_arg(args) {
//            Ok("open") => {
//                println!("compiling figure...");
//                let status = std::process::Command::new("dot")
//                    .args(["-Tpdf", "-o", "/tmp/figure.pdf", path])
//                    .status()?;
//                if !status.success() {
//                    return Err(ReplError::UnknownError);
//                }
//                println!("opening figure");
//                open::that("/tmp/figure.pdf")?;
//            }
//            Ok(cmd) => return Err(ReplError::UnknownCommand(cmd.to_string())),
//            Err(_) => {}
//        }
//
//        Ok(())
//    }
//
//    fn visualize_dn<'a>(
//        &mut self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        let spec = glas::DecodingNet::from_spec(&self.model).unwrap();
//        let path = Self::next_arg(args)?;
//
//        {
//            let mut file = std::fs::File::create(path)?;
//            output::glas::visualize_dn(&mut file, &spec)?;
//            println!("wrote graphviz description of GLAS to '{}'", path);
//        }
//
//        Self::open_if_desired(path, args)?;
//        Ok(())
//    }
//
//    fn visualize_glas<'a>(
//        &mut self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        match &self.glas {
//            None => self.derive_glas()?,
//            _ => (),
//        }
//        let (spec, glas) = self.glas.as_ref().unwrap();
//
//        let path = Self::next_arg(args)?;
//
//        {
//            let mut file = std::fs::File::create(path)?;
//            output::glas::visualize_glas(&mut file, &glas, &spec)?;
//            println!("wrote graphviz description of GLAS to '{}'", path);
//        }
//
//        Self::open_if_desired(path, args)?;
//        Ok(())
//    }
//
//    fn visualize_glas_for<'a>(
//        &mut self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        match &self.glas {
//            None => self.derive_glas()?,
//            _ => (),
//        }
//        let (spec, glas) = self.glas.as_ref().unwrap();
//
//        let (name, asid) = Self::next_asid(&self.model, args)?;
//        self.model
//            .contexts
//            .contains(&asid)
//            .then_some(())
//            .ok_or(ReplError::NotAContext(name.clone()))?;
//
//        let f = spec.flatten(&spec.contexts);
//        let path = Self::next_arg(args)?;
//        let mut file = std::fs::File::create(path)?;
//        output::glas::visualize_glas_for(
//            &mut file,
//            &glas,
//            &spec,
//            &f,
//            asid.as_usize(),
//            &spec.resources,
//        )?;
//        println!(
//            "wrote graphviz description of GLAS for context '{}' to '{}'",
//            name, path
//        );
//
//        Self::open_if_desired(path, args)?;
//        Ok(())
//    }
//
//    fn generate_page_table_for_context<'a>(
//        &mut self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        match &self.glas {
//            None => self.derive_glas()?,
//            _ => (),
//        }
//        let (spec, glas) = self.glas.as_ref().unwrap();
//        let f = spec.flatten(&spec.contexts);
//
//        let (name, asid) = Self::next_asid(&self.model, args)?;
//        self.model
//            .contexts
//            .contains(&asid)
//            .then_some(())
//            .ok_or(ReplError::NotAContext(name.clone()))?;
//
//        let base_addr = Self::next_num(args)?;
//
//        let path = Self::next_arg(args)?;
//        let mut file = std::fs::File::create(path)?;
//
//        eprintln!("deriving page table...");
//        let pt = glas::page_table_from_glas(
//            &glas,
//            &f,
//            asid.as_usize(),
//            base_addr,
//            &spec.resources,
//        );
//
//        eprintln!(
//            "number of pages used for page table: {} ({})",
//            pt.page_tracker.used_pages,
//            output::human_bytes(pt.page_tracker.used_pages as u128 * 4096)
//        );
//        let num_entries = pt.page_tracker.used_pages * 512;
//        let mut entries = vec![0; num_entries];
//        pt.base_table.materialize_into(&mut entries);
//
//        writeln!(file, "typedef unsigned long long pt_entry;")?;
//        writeln!(
//            file,
//            "unsigned long long num_page_table_entries = {:#x};",
//            num_entries
//        )?;
//        writeln!(
//            file,
//            "pt_entry *page_table_base = (pt_entry *)({:#x});",
//            base_addr
//        )?;
//        writeln!(file, "pt_entry page_table_entries[{}] = {{", num_entries)?;
//
//        let mut addr = base_addr;
//        for chunk in entries.chunks(8) {
//            if addr % 4096 == 0 {
//                writeln!(file, "  // ----------- {:#x} -------------", addr)?;
//            }
//            write!(file, " ")?;
//            for entry in chunk {
//                write!(file, " {:#x},", entry)?;
//                addr += 8;
//            }
//            writeln!(file)?;
//        }
//        writeln!(file, "}};")?;
//
//        writeln!(
//            file,
//            r#"
//void activate_page_tables() {{
//    for (int i = 0; i < num_page_table_entries; i++) {{
//        page_table_base[i] = page_table_entries[i];
//    }}
//    __asm__ ("LDR     x1, ={:#x}\n\t"   // program ttbr0 on this CPU
//             "MSR     ttbr0_el1, x1\n\t"
//             "LDR     x1, =0xff\n\t"         // program mair on this CPU
//             "MSR     mair_el1, x1\n\t"
//             "LDR     x1, =0x500803510\n\t"  // program tcr on this CPU
//             "MSR     tcr_el1, x1\n\t"
//             "ISB\n\t"
//             "MRS     x2, tcr_el1\n\t"       // verify CPU supports desired config
//             "CMP     x2, x1\n\t"
//             "B.NE    .\n\t"
//             "LDR     x1, =0x1005\n\t"       // program sctlr on this CPU
//             "MSR     sctlr_el1, x1\n\t"
//             "ISB\n\t"                       // synchronize context on this CPU
//             : : :
//    );
//}}
//"#,
//            base_addr
//        )?;
//        println!("wrote page tables to '{}'", path);
//        Ok(())
//    }
//
//
//    fn add_glas_constraint<'a>(
//        &mut self,
//        args: &mut impl Iterator<Item = &'a str>,
//    ) -> Result<(), ReplError> {
//        let (name, asid) = Self::next_asid(&self.model, args)?;
//        let phys_addr = Self::next_num(args)?;
//        let glas_addr = Self::next_num(args)?;
//        let len = Self::next_num(args)?;
//
//        let spec = glas::DecodingNet::from_spec(&self.model).unwrap();
//        let f = spec.flatten(&spec.contexts);
//        let mut is_valid_additional_constraint = false;
//        for &(src_range, _, _) in f.r[&asid.as_usize()].iter() {
//            if src_range.offset == phys_addr && src_range.length == len {
//                is_valid_additional_constraint = true;
//            }
//        }
//        if !is_valid_additional_constraint {
//            return Err(ReplError::InvalidGlasConstraint(
//                phys_addr, glas_addr, len,
//            ));
//        }
//
//        self.glas = None;
//        self.glas_constraints.push((
//            asid.as_usize(),
//            phys_addr,
//            glas_addr,
//            len,
//        ));
//        println!(
//            "added constraint to map {}@{:#x} to {:#x} in the GLAS",
//            name, phys_addr, glas_addr
//        );
//
//        Ok(())
//    }
//
