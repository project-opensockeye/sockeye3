use miette::Result;

mod cli;
mod decoding_net;
mod glas;
mod module;
mod output;
mod parser;
mod range;
mod specification;

fn main() -> Result<()> {
    cli::run()?;

    Ok(())
}
