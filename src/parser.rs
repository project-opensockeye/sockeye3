use std::collections::BTreeMap;
use std::fmt::Display;
use std::ops::{Deref, DerefMut};
use std::path::Path;
use std::sync::LazyLock;

use crate::decoding_net::{AddressSpaceAttribute, ResourceType};
use crate::range::Range;

use miette::{IntoDiagnostic, Result, SourceSpan};
use pest::{self, Parser};
use pest_derive::Parser;
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Located<T> {
    pub inner: T,
    pub location: Location,
}

impl<T> Located<T> {
    pub fn new(t: T, location: Location) -> Located<T> {
        Located { inner: t, location }
    }
}

impl<T> Deref for Located<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.inner
    }
}

impl<T> DerefMut for Located<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.inner
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Location {
    pub path: String,
    pub line: usize,
    pub col: usize,
    pub span: SourceSpan,
}

impl Display for Location {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}:{}", self.path, self.line, self.col)
    }
}

#[derive(Parser)]
#[grammar = "grammar.pest"]
struct ModuleParser;

#[derive(Debug)]
pub enum SoCExpression {
    Import(String, String),
    Instantiate(String, String, String),
    Connect(String, Option<Range>, String, Option<Range>, String),
}

#[derive(Debug)]
pub enum GlasExpression {
    ContextConstraint(String, Range, u128, String),
    ResourceConstraint(String, Range, u128, String),
}

pub fn parse_soc(path: &Path) -> Result<Vec<Located<SoCExpression>>> {
    let s = std::fs::read_to_string(path).into_diagnostic()?;
    let path = path.to_string_lossy().to_string();
    let pairs = ModuleParser::parse(Rule::SoC, &s).map_err(ParseError::from)?;
    let mut doc_comment = String::new();

    let mut exprs = Vec::new();
    for pair in pairs {
        let s = pair.as_span();
        let span = s.start()..s.end();
        let (line, col) = pair.line_col();

        let mut expr = None;
        let r = pair.as_rule();
        match r {
            Rule::DocCommentBody => {
                doc_comment += " ";
                doc_comment += pair.as_str().trim();
                doc_comment = doc_comment.trim().to_string();
                continue;
            }
            Rule::Import => {
                expr = Some(parse_import(
                    &mut pair.into_inner(),
                    doc_comment.clone(),
                )?);
            }
            Rule::Instantiate => {
                expr = Some(parse_instantiate(
                    &mut pair.into_inner(),
                    doc_comment.clone(),
                )?);
            }
            Rule::Connect => {
                expr = Some(parse_connect(
                    &mut pair.into_inner(),
                    doc_comment.clone(),
                )?);
            }
            Rule::EOI => (),
            _ => unreachable!(),
        }

        match expr {
            None => (),
            Some(expr) => exprs.push(Located {
                inner: expr,
                location: Location {
                    path: path.clone(),
                    span: span.into(),
                    line,
                    col,
                },
            }),
        }
    }

    Ok(exprs)
}

fn parse_import(
    it: &mut pest::iterators::Pairs<'_, Rule>,
    d: String,
) -> Result<SoCExpression> {
    let path = it.next().unwrap();
    assert!(path.as_rule() == Rule::Path);
    Ok(SoCExpression::Import(path.as_str().to_string(), d))
}

fn parse_instantiate(
    it: &mut pest::iterators::Pairs<'_, Rule>,
    d: String,
) -> Result<SoCExpression> {
    let inst_name = it.next().unwrap();
    assert!(inst_name.as_rule() == Rule::Ident);
    let mod_name = it.next().unwrap();
    assert!(mod_name.as_rule() == Rule::Ident);
    Ok(SoCExpression::Instantiate(
        inst_name.as_str().to_string(),
        mod_name.as_str().to_string(),
        d,
    ))
}

fn parse_connect(
    it: &mut pest::iterators::Pairs<'_, Rule>,
    d: String,
) -> Result<SoCExpression> {
    let out_port = it.next().unwrap();
    assert!(out_port.as_rule() == Rule::QualifiedIdent);

    let next = it.peek();
    let out_range = match next.map(|p| p.as_rule()) {
        Some(Rule::RangeExpr) => {
            Some(parse_range_expr(it.next().unwrap().into_inner())?)
        }
        _ => None,
    };

    let in_port = it.next().unwrap();
    assert!(in_port.as_rule() == Rule::QualifiedIdent);

    let next = it.peek();
    let in_range = match next.map(|p| p.as_rule()) {
        Some(Rule::RangeExpr) => {
            Some(parse_range_expr(it.next().unwrap().into_inner())?)
        }
        _ => None,
    };

    let o = out_port.as_str().to_string();
    let i = in_port.as_str().to_string();

    Ok(SoCExpression::Connect(o, out_range, i, in_range, d))
}

pub fn parse_glas(path: &Path) -> Result<Vec<Located<GlasExpression>>> {
    let s = std::fs::read_to_string(path).into_diagnostic()?;
    let path = path.to_string_lossy().to_string();
    let pairs =
        ModuleParser::parse(Rule::Glas, &s).map_err(ParseError::from)?;
    let mut doc_comment = String::new();

    let mut exprs = Vec::new();
    for pair in pairs {
        let s = pair.as_span();
        let span = s.start()..s.end();
        let (line, col) = pair.line_col();

        let mut expr = None;
        let r = pair.as_rule();
        match r {
            Rule::DocCommentBody => {
                doc_comment += " ";
                doc_comment += pair.as_str().trim();
                doc_comment = doc_comment.trim().to_string();
                continue;
            }
            Rule::ContextConstraint => {
                expr = Some(parse_context_constraint(
                    &mut pair.into_inner(),
                    doc_comment.clone(),
                )?);
            }
            Rule::ResourceConstraint => {
                expr = Some(parse_resource_constraint(
                    &mut pair.into_inner(),
                    doc_comment.clone(),
                )?);
            }
            Rule::EOI => (),
            _ => unreachable!(),
        }

        match expr {
            None => (),
            Some(expr) => exprs.push(Located {
                inner: expr,
                location: Location {
                    path: path.clone(),
                    span: span.into(),
                    line,
                    col,
                },
            }),
        }
    }

    Ok(exprs)
}

fn parse_context_constraint(
    it: &mut pest::iterators::Pairs<'_, Rule>,
    d: String,
) -> Result<GlasExpression> {
    let addr_space = it.next().unwrap();
    assert!(addr_space.as_rule() == Rule::QualifiedIdent);
    let addr_space = addr_space.as_str().to_string();

    let range = it.next().unwrap();
    assert!(range.as_rule() == Rule::RangeExpr);
    let range = parse_range_expr(range.into_inner())?;

    let addr = it.next().unwrap();
    assert!(addr.as_rule() == Rule::Number);
    let addr = parse_number(addr);

    Ok(GlasExpression::ContextConstraint(
        addr_space, range, addr, d,
    ))
}

fn parse_resource_constraint(
    it: &mut pest::iterators::Pairs<'_, Rule>,
    d: String,
) -> Result<GlasExpression> {
    let addr_space = it.next().unwrap();
    assert!(addr_space.as_rule() == Rule::QualifiedIdent);
    let addr_space = addr_space.as_str().to_string();

    let range = it.next().unwrap();
    assert!(range.as_rule() == Rule::RangeExpr);
    let range = parse_range_expr(range.into_inner())?;

    let addr = it.next().unwrap();
    assert!(addr.as_rule() == Rule::Number);
    let addr = parse_number(addr);

    Ok(GlasExpression::ResourceConstraint(
        addr_space, range, addr, d,
    ))
}

#[derive(Debug)]
pub enum ModuleExpression {
    AddressSpace(String, u128, AddressSpaceAttribute, String),
    Mapping(String, String, u128, u128, u128, String),
    InPort(String, Option<(String, Range)>, String),
    OutPort(String, Option<(String, Range)>, String),
}

pub fn parse_module(path: &Path) -> Result<Vec<Located<ModuleExpression>>> {
    let s = std::fs::read_to_string(path).into_diagnostic()?;
    let path = path.to_string_lossy().to_string();
    let pairs =
        ModuleParser::parse(Rule::Module, &s).map_err(ParseError::from)?;

    let mut exprs = Vec::new();
    let mut doc_comment = "".to_string();
    for pair in pairs {
        let s = pair.as_span();
        let span = s.start()..s.end();
        let (line, col) = pair.line_col();

        let mut expr = None;
        let r = pair.as_rule();
        match r {
            Rule::DocCommentBody => {
                doc_comment += " ";
                doc_comment += pair.as_str().trim();
                doc_comment = doc_comment.trim().to_string();
            }
            Rule::AddressSpace => {
                expr = Some(parse_address_space(
                    &mut pair.into_inner(),
                    doc_comment.clone(),
                )?);
                doc_comment.clear();
            }
            Rule::Mapping => {
                expr = Some(parse_mapping(
                    &mut pair.into_inner(),
                    doc_comment.clone(),
                )?);
                doc_comment.clear();
            }
            Rule::Port => {
                expr = Some(parse_port(
                    &mut pair.into_inner(),
                    doc_comment.clone(),
                )?);
                doc_comment.clear();
            }
            Rule::EOI => (),
            _ => unreachable!(),
        }

        match expr {
            None => (),
            Some(expr) => exprs.push(Located {
                inner: expr,
                location: Location {
                    path: path.clone(),
                    span: span.into(),
                    line,
                    col,
                },
            }),
        }
    }

    Ok(exprs)
}

fn parse_address_space(
    it: &mut pest::iterators::Pairs<'_, Rule>,
    doc_string: String,
) -> Result<ModuleExpression> {
    let attr = parse_address_space_type(it.next().unwrap());
    let name = it.next().unwrap().as_str();
    let r = it.next().unwrap();
    let size = parse_size(r)?;

    Ok(ModuleExpression::AddressSpace(
        name.to_string(),
        size,
        attr,
        doc_string,
    ))
}

fn parse_mapping(
    it: &mut pest::iterators::Pairs<'_, Rule>,
    doc_string: String,
) -> Result<ModuleExpression> {
    let src = it.next().unwrap().as_str();

    let p = it.next().unwrap();
    let (src_offset, mut length) = match p.as_rule() {
        Rule::RangeExpr => {
            let r = parse_range_expr(p.into_inner())?;
            (r.start(), r.length())
        }
        Rule::Number => (parse_number(p), 0),
        _ => unreachable!(),
    };

    let dst = it.next().unwrap().as_str();
    let dst_offset = parse_number(it.next().unwrap());

    if length == 0 {
        length = parse_size(it.next().unwrap())?;
    }

    Ok(ModuleExpression::Mapping(
        src.to_string(),
        dst.to_string(),
        src_offset,
        dst_offset,
        length,
        doc_string,
    ))
}

fn parse_port(
    it: &mut pest::iterators::Pairs<'_, Rule>,
    doc_string: String,
) -> Result<ModuleExpression> {
    let port_type = it.next().unwrap().as_str();
    let name = it.next().unwrap().as_str();

    let port_type = match port_type {
        "in" => ModuleExpression::InPort,
        "out" => ModuleExpression::OutPort,
        _ => unreachable!(),
    };

    let address_space = it.next().map(|p| p.as_str().to_string());
    let range = it
        .next()
        .map(|p| parse_range_expr(p.into_inner()))
        .transpose()?;

    Ok(port_type(
        name.to_string(),
        address_space.and_then(|n| range.map(|r| (n, r))),
        doc_string,
    ))
}

fn parse_range_expr(mut it: pest::iterators::Pairs<'_, Rule>) -> Result<Range> {
    let start_addr = parse_number(it.next().unwrap());
    let next = it.next().unwrap();

    let range = match next.as_rule() {
        Rule::Number => {
            let end_addr = parse_number(next);
            let len = end_addr - start_addr + 1;
            Range::new(start_addr, len)
        }
        Rule::Size => {
            let size = parse_size(next)?;
            Range::new(start_addr, size)
        }
        _ => unreachable!(),
    };

    Ok(range)
}

fn parse_number(p: pest::iterators::Pair<'_, Rule>) -> u128 {
    assert!(p.as_rule() == Rule::Number);
    let s = p.as_str().replace("_", "");
    let radix = if s.starts_with("0x") { 16 } else { 10 };
    let s = s.trim_start_matches("0x");
    u128::from_str_radix(s, radix).unwrap()
}

static SIZE_SUFFIX: LazyLock<BTreeMap<&'static str, Suffix>> =
    LazyLock::new(|| {
        BTreeMap::from([
            ("bits", Suffix::Bits),
            ("B", Suffix::Bytes(1)),
            ("kiB", Suffix::Bytes(1 << 10)),
            ("MiB", Suffix::Bytes(1 << 20)),
            ("GiB", Suffix::Bytes(1 << 30)),
            ("TiB", Suffix::Bytes(1 << 40)),
            ("PiB", Suffix::Bytes(1 << 50)),
            ("EiB", Suffix::Bytes(1 << 60)),
        ])
    });

#[derive(Clone, Copy, Debug, Default)]
enum Suffix {
    #[default]
    None,
    Bytes(u128),
    Bits,
}

fn parse_size_suffix(s: Option<pest::iterators::Pair<'_, Rule>>) -> Suffix {
    let s = s.map(|s| s.as_str()).unwrap_or_default();
    *(SIZE_SUFFIX.get(s).unwrap_or(&Suffix::None))
}

fn parse_size(it: pest::iterators::Pair<'_, Rule>) -> Result<u128> {
    assert!(it.as_rule() == Rule::Size);
    let mut it = it.into_inner();
    let num = parse_number(it.next().unwrap());
    Ok(match parse_size_suffix(it.next()) {
        Suffix::None => num,
        Suffix::Bytes(b) => num * b,
        Suffix::Bits => 1 << num,
    })
}

fn parse_address_space_type(
    r: pest::iterators::Pair<'_, Rule>,
) -> AddressSpaceAttribute {
    assert!(r.as_rule() == Rule::AddressSpaceType);
    match r.as_str() {
        "as" => AddressSpaceAttribute::None,
        "ctx" => AddressSpaceAttribute::Context,
        "dev" => AddressSpaceAttribute::Resource(ResourceType::Device),
        "res" => AddressSpaceAttribute::Resource(ResourceType::Unknown),
        "mem" => AddressSpaceAttribute::Resource(ResourceType::Memory),
        _ => unreachable!(),
    }
}

#[derive(Debug, Error)]
pub enum ParseError {
    #[error("Failed to parse")]
    PestError(pest::error::Error<Rule>),
}

impl From<pest::error::Error<Rule>> for ParseError {
    fn from(err: pest::error::Error<Rule>) -> Self {
        Self::PestError(err)
    }
}

impl miette::Diagnostic for ParseError {
    fn code<'a>(&'a self) -> Option<Box<dyn Display + 'a>> {
        Some(Box::new("parser::failure"))
    }

    fn help<'a>(&'a self) -> Option<Box<dyn std::fmt::Display + 'a>> {
        match self {
            ParseError::PestError(err) => {
                let mut help = String::new();
                match &err.variant {
                    pest::error::ErrorVariant::ParsingError {
                        positives,
                        negatives,
                    } => {
                        if !positives.is_empty() {
                            help += "Expected one of: ";
                            for (i, p) in positives.iter().enumerate() {
                                if i > 0 {
                                    help += ", ";
                                }
                                help += format!("{:?}", p).as_str();
                            }
                        }
                        if !negatives.is_empty() {
                            if !help.is_empty() {
                                help += "; ";
                            }
                            help += "Negatives: ";
                            for (i, n) in negatives.iter().enumerate() {
                                if i > 0 {
                                    help += ", ";
                                }
                                help += format!("{:?}", n).as_str();
                            }
                        }
                        if help.is_empty() {
                            return None;
                        }
                    }
                    pest::error::ErrorVariant::CustomError { message } => {
                        help += message;
                    }
                }
                Some(Box::new(help))
            }
        }
    }

    fn labels(
        &self,
    ) -> Option<Box<dyn Iterator<Item = miette::LabeledSpan> + '_>> {
        match self {
            ParseError::PestError(err) => {
                let (start, len) = match err.location {
                    pest::error::InputLocation::Pos(start) => (start, 1),
                    pest::error::InputLocation::Span((start, end)) => {
                        (start, end - start)
                    }
                };

                Some(Box::new(std::iter::once(miette::LabeledSpan::new(
                    Some("here".to_owned()),
                    start,
                    len,
                ))))
            }
        }
    }
}
