use std::collections::BTreeMap;

use crate::decoding_net::Context;
use crate::range::*;

use miette::Result;

type LinearAddressMap = RangeTree<(String, Range)>;
pub type FlattenedDecodingNet = BTreeMap<String, LinearAddressMap>;

impl Splittable for (String, Range) {
    fn split(&mut self, len: u128) -> Self {
        let right = self.1.split(len);

        (self.0.clone(), right)
    }
}

impl super::DecodingNet {
    /// Flattens this decoding net, returning a linear list of address spaces
    /// for each decoding net,
    pub fn flatten_contexts(&self) -> miette::Result<FlattenedDecodingNet> {
        let mut flattened_contexts = BTreeMap::new();

        for a in self.iter_contexts() {
            // TODO: allow contexts to specify a default "initial context" or
            // set of initial contexts
            let rt = self.view(&a.name, Context::new(), None)?;
            flattened_contexts.insert(a.name.clone(), rt);
        }

        Ok(flattened_contexts)
    }

    /// Provide a view of the specified depth from that particular address
    /// space.
    pub fn view(
        &self,
        addr_space: &str,
        context: Context,
        depth: Option<usize>,
    ) -> Result<LinearAddressMap> {
        let a = &self.address_spaces[addr_space];
        let mut rt = RangeTree::new(a.range);
        for (r, _) in a.mappings.iter() {
            let flattened =
                self.recursive_flatten(&a.name, context.clone(), depth, r)?;

            for (original_range, vs) in flattened {
                for (target_as, dst_range) in vs {
                    rt.assign(original_range, (target_as, dst_range));
                }
            }
        }

        Ok(rt)
    }

    /// A recursive flattening of range in addr_space. Produces a list of tuples
    /// (input range, output address space, output range) pairs, with the
    /// guarantees that input and output ranges have the same length.
    fn recursive_flatten(
        &self,
        addr_space: &str,
        context: Context,
        depth: Option<usize>,
        range: Range,
    ) -> Result<RangeTree<(String, Range)>> {
        // If the requested depth is zero, then we return a range tree
        // that points to this level.
        if depth == Some(0) {
            let mut rt = RangeTree::new(range);
            rt.assign(range, (addr_space.to_string(), range));
            return Ok(rt);
        }

        // first, we determine the "maximum" set of ranges within r
        // so it can look like this:
        //       |        r           |
        // |    s    |     ..    |    e       |
        let it = self.address_spaces[addr_space].mappings.iter_range(range);

        // In total, we want to flatten `len` bytes at `off`
        let mut off = range.offset();
        let mut len = range.length();
        let mut result = RangeTree::null(off);
        for (subrange, map) in it {
            // Once we have no remaining bytes left, we can break.
            if len == 0 {
                break;
            }

            // Account for the fact that our offset off might start later than
            // the current subrange
            //         | <- off
            // |    subrange            |
            assert!(subrange.offset() <= off);
            let offset_diff = off - subrange.offset();

            // We want to map until the end of subrange, however we need to
            // adjust this length by offset_diff in order to account for a
            // potential offset (see above).
            let mut llen = subrange.length();
            llen -= offset_diff;

            // Also, we have to account for the fact that this range might
            // exceed our desired flattening length.
            //                | <- len
            // |        subrange                |
            llen = u128::min(len, llen);

            // Now, depending on whether we have a mapping here, we do the
            // following. Note that we are guaranteed that subrange has the
            // _exact_ length of a mapping.
            let flattened_subrange = Range::new(off, llen);
            let mut res = RangeTree::new(flattened_subrange);
            let mut off2 = off;
            let mut len2 = llen;
            let mut was_mapped = false;
            for m in map {
                if !context.matches_all(&m.conditions) {
                    continue;
                }

                was_mapped = true;
                let mut new_context = context.clone();
                new_context.apply_all(&m.modifications)?;

                let asdf = self.recursive_flatten(
                    &m.dst,
                    new_context,
                    depth.map(|d| d - 1),
                    Range::new(m.dst_range.offset() + offset_diff, llen),
                )?;

                for (r, vs) in asdf {
                    let current_range =
                        Range::new(off2, u128::min(len2, r.length()));
                    res.split_at(current_range.end());

                    for v in vs {
                        res.assign(current_range, v);
                    }

                    off2 += u128::min(len, r.length());
                    len2 -= u128::min(len, r.length());
                }
            }

            if !was_mapped {
                res.assign(
                    flattened_subrange,
                    (addr_space.to_string(), flattened_subrange),
                );
            }

            off += llen;
            len -= llen;

            result.concat(res)?;
        }

        Ok(result)
    }
}
