use std::collections::BTreeMap;
use std::fmt::Debug;

use crate::parser::{Located, Location};
use crate::range::*;

use serde::{Deserialize, Serialize};

use miette::{miette, LabeledSpan, Result};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
#[allow(dead_code)]
pub enum AddressSpaceAttribute {
    None,
    Context,
    Resource(ResourceType),
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ResourceType {
    Device,
    Memory,
    Unknown,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[allow(dead_code)]
pub struct AddressSpace {
    pub name: String,
    pub description: String,
    pub range: Range,
    pub mappings: RangeTree<Located<Mapping>>,
    pub attribute: AddressSpaceAttribute,
}

impl AddressSpace {
    pub fn new(
        name: &str,
        size_bytes: u128,
        description: &str,
        attribute: AddressSpaceAttribute,
    ) -> AddressSpace {
        let r = Range::new(0, size_bytes);
        AddressSpace {
            name: name.to_string(),
            description: description.to_string(),
            range: r,
            mappings: RangeTree::new(r),
            attribute,
        }
    }

    pub fn is_context(&self) -> bool {
        matches!(self.attribute, AddressSpaceAttribute::Context)
    }

    pub fn is_resource(&self) -> bool {
        matches!(self.attribute, AddressSpaceAttribute::Resource(_))
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Condition {
    key: String,
    value: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Modification {
    Add(String, String),
    Update(String, String),
    Remove(String),
}

#[derive(Debug, Clone)]
pub struct Context {
    inner: BTreeMap<String, String>,
}

impl Context {
    pub fn new() -> Context {
        Context {
            inner: BTreeMap::new(),
        }
    }

    pub fn matches(&self, c: &Condition) -> bool {
        self.inner.get(&c.key) == Some(&c.value)
    }

    pub fn matches_all(&self, cs: &[Condition]) -> bool {
        cs.iter().all(|c| self.matches(c))
    }

    pub fn apply(&mut self, m: &Modification) -> Result<()> {
        let ok = match m {
            Modification::Add(key, value) => self
                .inner
                .insert(key.to_string(), value.to_string())
                .is_none(),
            Modification::Update(key, value) => self
                .inner
                .insert(key.to_string(), value.to_string())
                .is_some(),
            Modification::Remove(key) => self.inner.remove(key).is_some(),
        };

        if !ok {
            return Err(miette!("decoding_net::context::apply"));
        }

        Ok(())
    }

    pub fn apply_all(&mut self, ms: &Vec<Modification>) -> Result<()> {
        for m in ms {
            self.apply(m)?;
        }

        Ok(())
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Mapping {
    pub conditions: Vec<Condition>,
    pub modifications: Vec<Modification>,

    pub dst: String,
    pub dst_range: Range,

    pub description: String,
}

impl Splittable for Mapping {
    fn split(&mut self, len: u128) -> Self {
        let mut right_mapping = self.clone();
        let right = self.dst_range.split(len);

        right_mapping.dst_range = right;
        right_mapping
    }
}

impl Splittable for Located<Mapping> {
    fn split(&mut self, len: u128) -> Self {
        let right = self.inner.split(len);

        Located {
            location: self.location.clone(),
            inner: right,
        }
    }
}

#[derive(Debug, Clone)]
pub struct DecodingNet {
    pub address_spaces: BTreeMap<String, Located<AddressSpace>>,
}

impl DecodingNet {
    pub fn new() -> DecodingNet {
        DecodingNet {
            address_spaces: BTreeMap::new(),
        }
    }

    pub fn range_of(&mut self, name: &str) -> Result<Range> {
        let address_space = self.ensure_address_space_exists(name)?;
        Ok(address_space.range)
    }

    pub fn is_resource(&self, id: &str) -> Result<bool> {
        let address_space = self.ensure_address_space_exists(id)?;
        Ok(matches!(
            address_space.attribute,
            AddressSpaceAttribute::Resource(_)
        ))
    }

    pub fn add_address_space<'a>(
        &mut self,
        name: &'a str,
        description: &str,
        range: Range,
        attribute: AddressSpaceAttribute,
        location: Location,
    ) -> Result<&'a str> {
        match self.address_spaces.get(name) {
            None => (),
            Some(addr_space) => {
                return Err(miette!(
                    code = "decoding_net::name_already_taken",
                    help = format!(
                        "previous definition at {}",
                        addr_space.location
                    ),
                    labels = vec![LabeledSpan::at(
                        location.span,
                        "second usage here"
                    )],
                    "address space {name} already exists"
                ));
            }
        }

        self.address_spaces.insert(
            name.to_string(),
            Located::new(
                AddressSpace::new(name, range.length(), description, attribute),
                location,
            ),
        );

        Ok(name)
    }

    pub fn ensure_address_space_exists(
        &self,
        name: &str,
    ) -> Result<&Located<AddressSpace>> {
        self.address_spaces.get(name).ok_or(miette!(
            code = "decoding_net::address_space_does_not_exist",
            "address space {name} does not exist"
        ))
    }

    pub fn add_mapping(
        &mut self,
        src: &str,
        dst: &str,

        src_range: Range,
        dst_range: Range,

        description: &str,
        location: Location,
    ) -> Result<()> {
        let src_as = self.ensure_address_space_exists(src)?;
        let dst_as = self.ensure_address_space_exists(dst)?;

        dbg!(&self);
        if !src_as.range.contains(&src_range) {
            return Err(miette!(
                help = format!("actual range {}", src_as.range),
                "requested mapping range outside of address space {src}, defined in {}",
                src_as.location,
            ));
        }

        if !dst_as.range.contains(&dst_range) {
            return Err(miette!(
                help = format!("actual range {}, requested range {dst_range}", dst_as.range),
                labels = vec![LabeledSpan::at(location.span, "here")],
                "requested mapping range outside of address space {dst}, defined in {}",
                dst_as.location,
            ));
        }

        let m = Mapping {
            conditions: Vec::new(),
            modifications: Vec::new(),

            dst: dst.to_string(),
            dst_range,
            description: description.to_string(),
        };
        let src_as = self.address_spaces.get_mut(src).unwrap();
        src_as
            .mappings
            .assign(src_range, Located::new(m, location.clone()));

        Ok(())
    }

    /// Iterate over the contexts
    pub fn iter_contexts(
        &self,
    ) -> impl Iterator<Item = &Located<AddressSpace>> {
        self.address_spaces
            .values()
            .filter(|a| matches!(a.attribute, AddressSpaceAttribute::Context))
    }

    /// Add the given decoding net to self, prefixing all address space
    /// identifiers with `prefix`.
    pub fn add_prefixed_subnet(
        &mut self,
        prefix: &str,
        dn: &DecodingNet,
        location: Location,
    ) -> Result<()> {
        for (name, a) in dn.address_spaces.iter() {
            self.add_address_space(
                &format!("{prefix}.{name}"),
                &a.description,
                a.range,
                a.attribute,
                location.clone(),
            )?;
        }

        for (src, a) in dn.address_spaces.iter() {
            for (src_range, ms) in a.mappings.iter() {
                for m in ms {
                    self.add_mapping(
                        &format!("{prefix}.{}", src),
                        &format!("{prefix}.{}", m.dst),
                        src_range,
                        m.dst_range,
                        &m.description,
                        location.clone(),
                    )?;
                }
            }
        }

        Ok(())
    }
}
