use std::fs::File;
use std::io::Result;

use crate::decoding_net::{AddressSpaceAttribute, DecodingNet};
use crate::glas::Glas;

use serde_json::{json, Map, Value};

/// Output a JSON description of the GLAS, and the mappings of the individual
/// context's address spaces.
///
///
pub fn write_json(
    file: &mut File,
    dn: &DecodingNet,
    glas: &Glas,
) -> Result<()> {
    let mut per_context_views = Map::new();

    for (ctx, space) in glas.contexts.iter() {
        let mut ctx_view = Vec::new();

        for (ctx_range, mapped) in space.iter() {
            for (res, resource_range) in mapped {
                let i = glas
                    .placement
                    .iter()
                    .position(|(r, _, c)| r == res && c == resource_range);

                let i = match i {
                    Some(i) => i,
                    None => continue,
                };

                let (_, glas_range, _) = glas.placement[i];

                let entry = json!({
                    "id": res,
                    "name": res,
                    "begin_phys": ctx_range.start(),
                    "end_phys": ctx_range.start(),
                    "begin_glas": glas_range.start(),
                    "end_glas": glas_range.end(),
                    "size": ctx_range.length(),
                    "resource_type": match dn.address_spaces[res].attribute {
                        AddressSpaceAttribute::Resource(ty) => format!("{ty:?}"),
                        _ => "Invalid".to_string(),
                    },
                    "comment": "",
                });
                ctx_view.push(entry);
            }
        }

        per_context_views.insert(ctx.to_string(), Value::Array(ctx_view));
    }

    let mut glas_json = Vec::new();
    for (resource, glas_range, resource_range) in glas.placement.iter() {
        let entry = json!({
            "id": resource,
            "name": resource,
            "begin_glas": glas_range.start(),
            "end_glas": glas_range.end(),
            "begin_resource": resource_range.start(),
            "end_resource": resource_range.end(),
            "size": resource_range.length(),
            "comment": "",
        });
        glas_json.push(entry);
    }

    serde_json::to_writer_pretty(
        file,
        &json!({
            "per_context_views": per_context_views,
            "glas": glas_json,
        }),
    )?;
    Ok(())
}
