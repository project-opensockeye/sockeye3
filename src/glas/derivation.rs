//! The GLAS module is responsible for deriving a global logical address space
//! from a decoding net specification.
//!
//! A GLAS is a virtual address space that places each resource at a unique
//! address, trying to optimize a combination of constraints:
//!
//!  * satisfy the hard user constraints
//!  * minimize the amount of shifts necessary
//!  * ...

use std::collections::BTreeMap;
use std::io::Write;
use std::path::Path;

use log::*;

use crate::decoding_net::{
    AddressSpaceAttribute, DecodingNet, FlattenedDecodingNet,
};
use crate::parser::{self, Located};
use crate::range::Range;
//use crate::specs::examples::mmu::PageTable;

use serde::{Deserialize, Serialize};

use miette::{miette, IntoDiagnostic, LabeledSpan, NamedSource, Result};

/// Represents a global logical address space, computed for a particular
/// decoding net.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Glas {
    /// Records the placement of a particular resource range in a contiguous
    /// (offset, length) region. For more efficient range lookups, we should use
    /// something akin to an interval tree.
    pub placement: Vec<(String, Range, Range)>,

    /// Records the individual offsets that a context c has to apply to reach a
    /// particular resource.
    pub swizzles: Vec<(String, Range, i128)>,

    /// Greatest power of two that all addresses of a GLAS are aligned to.
    pub alignment: u128,

    pub contexts: FlattenedDecodingNet,
    pub address_spaces: BTreeMap<String, AddressSpaceAttribute>,
}

#[derive(Debug, Clone)]
pub struct GlasConstraint {
    context: String,
    range: Range,
    offset: u128,
}

#[derive(Debug, Clone)]
pub struct GlasDerivationOptions {
    /// A list of exact constraints that must be fulfilled.
    pub constraints: Vec<Located<GlasConstraint>>,

    /// Attempt an optimal solution, minimizing the number of non-zero offsets.
    pub try_optimal: bool,
}

impl GlasDerivationOptions {
    /// Parse the GLAS deriviation constraints from the given path.
    pub fn parse_constraints(
        path: &Path,
        dn: &DecodingNet,
    ) -> Result<Vec<Located<GlasConstraint>>> {
        let mut constraints = Vec::new();

        let _name = path.file_stem();
        let named_source = NamedSource::new(
            path.to_string_lossy(),
            std::fs::read_to_string(path).unwrap(),
        );
        let exprs = parser::parse_glas(path)
            .map_err(|err| err.with_source_code(named_source.clone()))?;

        let f = dn.flatten_contexts()?;
        for expr in exprs {
            match expr.inner {
                parser::GlasExpression::ContextConstraint(
                    context,
                    range,
                    offset,
                    _doc_comment,
                ) => {
                    if !dn.ensure_address_space_exists(&context)?.is_context() {
                        return Err(miette!(
                            code = "glas::constraint::not_a_context",
                            labels = vec![LabeledSpan::at(
                                expr.location.span,
                                "here",
                            )],
                            "asdf"
                        ));
                    }

                    constraints.push(Located::new(
                        GlasConstraint {
                            context,
                            range,
                            offset,
                        },
                        expr.location,
                    ));
                }
                parser::GlasExpression::ResourceConstraint(
                    resource,
                    range,
                    glas_address,
                    _doc_comment,
                ) => {
                    if !dn.ensure_address_space_exists(&resource)?.is_resource()
                    {
                        return Err(miette!(
                            code = "glas::constraint::not_a_resource",
                            labels = vec![LabeledSpan::at(
                                expr.location.span,
                                "here",
                            )],
                            "asdf"
                        ));
                    }

                    for (ctx, space) in f.iter() {
                        for (src_range, ms) in space.iter() {
                            for (_, resource_range) in ms
                                .iter()
                                .filter(|(r, _)| *r == resource)
                                .filter(|(_, rng)| range.overlaps(rng))
                            {
                                let start_diff = range.start() as i128
                                    - resource_range.start() as i128;

                                let end_diff = range.end() as i128
                                    - resource_range.end() as i128;

                                let ctx_range_start =
                                    (src_range.start() as i128 + start_diff)
                                        as u128;
                                let ctx_range_end = (src_range.end() as i128
                                    + end_diff)
                                    as u128;
                                let ctx_range = Range::new(
                                    ctx_range_start,
                                    ctx_range_end - ctx_range_start,
                                );
                                constraints.push(Located::new(
                                    GlasConstraint {
                                        context: ctx.to_string(),
                                        range: ctx_range,
                                        offset: glas_address,
                                    },
                                    expr.location.clone(),
                                ))
                            }
                        }
                    }
                }
            }
        }

        eprintln!("{:#?}", &constraints);
        Ok(constraints)
    }
}

/// Derive the GLAS for the given decoding net, contexts, and resources.
///
/// The result contains a list of placements of resource ranges in the GLAS, as
/// well as a list of offsets that must be applied locally.
///
/// The GLAS is computed by Z3 via encoding it as an optimization problem with
/// suitable constraints.
pub fn derive_glas(
    dn: &DecodingNet,
    options: &GlasDerivationOptions,
) -> Result<Glas> {
    let alignment = greatest_common_alignment(dn, &options.constraints);

    if alignment < 10 {
        warn!("alignment < 1024 detected, are you sure that this is ok?",);
    }

    let mut glas = Glas::new(1 << alignment);
    glas.address_spaces = dn
        .address_spaces
        .iter()
        .map(|(n, a)| (n.to_string(), a.attribute))
        .collect();

    let cfg = z3::Config::new();
    let ctx = z3::Context::new(&cfg);

    let mut buf = Vec::new();
    let (loffs, goffs, f) = write_smtlib_program_to(dn, options, &mut buf)?;
    let prog = String::from_utf8(buf).unwrap();
    glas.contexts = f;

    debug!("initializing solver");
    let opt = z3::Optimize::new(&ctx);
    debug!("importing program");
    debug!(
        "vars={} (goffs={} loffs={})",
        goffs.len() + loffs.len(),
        goffs.len(),
        loffs.len(),
    );
    opt.from_string(prog.clone());

    // TODO: figure out why we can't set the maxsat_engine to wmax here
    // the thing gets a lot faster then.
    match opt.check(&[]) {
        z3::SatResult::Unsat => {
            let solver = z3::Solver::new(&ctx);
            solver.from_string("(set-option :produce_unsat_core true)");
            solver.from_string(prog);
            solver.check();
            let core = solver.get_unsat_core();
            dbg!(core);
            return Err(miette!(code = "glas::z3::unsat", ""));
        }
        z3::SatResult::Unknown => {
            return Err(miette!(code = "glas::z3::unknown", ""));
        }
        z3::SatResult::Sat => (),
    };

    let model = opt.get_model().unwrap();
    let objs = opt.get_objectives();
    let _stats = opt.get_statistics();

    for obj in objs.iter() {
        debug!(
            "number of swizzles necessary: {}",
            model.eval(obj, true).unwrap().as_real().unwrap(),
        );
    }

    for (go, &(ref r, ro, rl)) in goffs.iter() {
        let astnode = z3::ast::Int::new_const(&ctx, go.clone());
        let off = model.get_const_interp(&astnode).unwrap().as_u64().unwrap();
        glas.placement.push((
            r.to_string(),
            Range::new(off as u128 * glas.alignment, rl),
            Range::new(ro, rl),
        ));
    }

    for (lo, &(ref c, co, cl)) in loffs.iter() {
        let astnode = z3::ast::Int::new_const(&ctx, lo.clone());
        let swz = match model.get_const_interp(&astnode) {
            None => 0,
            Some(num) => num.as_i64().unwrap(),
        };

        if swz == 0 {
            continue;
        }

        glas.swizzles.push((
            c.to_string(),
            Range::new(co, cl),
            swz as i128 * glas.alignment as i128,
        ));
    }

    glas.placement.sort_by_key(|b| b.1.offset());
    glas.swizzles.sort_by_key(|b| (b.0.clone(), b.1.offset()));
    Ok(glas)
}

fn range_alignment(r: Range) -> u32 {
    u32::min(r.offset().trailing_zeros(), r.length().trailing_zeros())
}

fn greatest_common_alignment(
    dn: &DecodingNet,
    additional_constraints: &[Located<GlasConstraint>],
) -> u32 {
    let mut best_alignment: u32 = 63;

    for (_, a) in dn.address_spaces.iter() {
        for (r, _) in a.mappings.iter() {
            best_alignment = u32::min(best_alignment, range_alignment(r));
        }
    }

    for gc in additional_constraints.iter() {
        let pa = gc.range.offset();
        let ga = gc.offset;
        let len = gc.range.length();
        let a = u32::min(
            len.trailing_zeros(),
            u32::min(pa.trailing_zeros(), ga.trailing_zeros()),
        );

        best_alignment = u32::min(best_alignment, a);
    }

    best_alignment
}

#[allow(clippy::type_complexity)]
fn write_smtlib_program_to<W: Write>(
    dn: &DecodingNet,
    options: &GlasDerivationOptions,
    w: &mut W,
) -> Result<(
    BTreeMap<String, (String, u128, u128)>,
    BTreeMap<String, (String, u128, u128)>,
    FlattenedDecodingNet,
)> {
    let alignment: u128 =
        1 << greatest_common_alignment(dn, &options.constraints);
    debug!("best alignment = {:#x}", alignment);
    let mut vars = BTreeMap::new();

    let mut loffs = BTreeMap::new();
    let mut goffs = BTreeMap::new();

    debug!("generating smtlib program");
    writeln!(w, "(set-logic QF_LIA)").into_diagnostic()?;

    let mut f = dn.flatten_contexts()?;
    debug!("splitting thee flattened decoding net for constraints");
    for gc in options.constraints.iter() {
        // we want phys_addr, len to map to glas_addr, len
        let rt = f.get_mut(&gc.context).unwrap();
        rt.split_at(gc.range.start());
        rt.split_at(gc.range.end());
    }

    for (c, space) in f.iter() {
        for (src_range, rs) in space.iter() {
            for (r, dst_range) in rs {
                if !dn.is_resource(r)? {
                    continue;
                }

                // add variables for individual offsets of each resource
                let o = format!("o_{}_{:x}", c, src_range.offset());
                loffs.insert(
                    o.to_string(),
                    (c.to_string(), src_range.offset(), src_range.length()),
                );
                writeln!(w, "(declare-const {o} Int)").into_diagnostic()?;

                let ro = dst_range.offset();
                let rl = dst_range.length();
                let go = format!("go_{r}_{ro:x}_{rl}");

                let e =
                    vars.entry((r.clone(), ro, rl, go)).or_insert(Vec::new());
                e.push((c, src_range, r.clone(), dst_range, o));
            }
        }
    }

    debug!("declared offset vars");
    for ((_r, _ro, _rl, go), _) in vars.iter() {
        goffs.insert(go.to_string(), (_r.to_string(), *_ro, *_rl));
        writeln!(w, "(declare-const {go} Int)").into_diagnostic()?;
    }
    debug!("declared global offsets");

    for ((_r, _ro, _rl, go), _) in vars.iter() {
        writeln!(w, "(assert (<= 0 {go}))").into_diagnostic()?;
    }
    debug!("constrained global offsets to be positive");

    if options.try_optimal {
        for (_, res) in vars.iter() {
            for (_, _, _, _, o) in res {
                writeln!(w, "(assert-soft (= 0 {o}))").into_diagnostic()?;
            }
        }
        debug!("wrote soft constraint for reducing number of non-zero offsets");
    }

    // add constraints for each of the offsets;
    for ((_r, _ro, rl, go), res) in vars.iter() {
        for (_c, cr, _r, _rr, o) in res {
            let co = cr.offset();
            let co = co / alignment;
            // we already know that rl and cr.length() match;
            assert!(*rl == cr.length());
            writeln!(w, "(assert (= {go} (+ {co} {o})))").into_diagnostic()?;
        }
    }
    debug!("wrote constraints for local-to-global offsets");

    debug!("declaring additional constriants");
    for gc in options.constraints.iter() {
        let name = &gc.context;
        let phys_addr = gc.range.offset();
        let glas_addr = gc.offset;

        let o = format!("o_{}_{:x}", name, phys_addr);
        let o_value: i128 =
            (glas_addr / alignment) as i128 - (phys_addr / alignment) as i128;
        writeln!(w, "(assert (= {o} {o_value}))").into_diagnostic()?;
    }

    debug!(
        "writing disjointness constraints num={}",
        vars.keys().count(),
    );
    for ((r1, ro1, rl1, go1), _) in vars.iter() {
        for ((r2, ro2, rl2, go2), _) in vars.iter() {
            if r1 == r2 && ro1 == ro2 && rl1 == rl2 {
                continue;
            }
            let rl1 = rl1 / alignment;
            let rl2 = rl2 / alignment;
            writeln!(
                w,
                "(assert (or\n  (<= (+ {go1} {rl1}) {go2})\n  (<= (+ {go2} {rl2}) {go1})))"
            ).into_diagnostic()?;
        }
    }

    Ok((loffs, goffs, f))
}

impl Glas {
    fn new(alignment: u128) -> Glas {
        Glas {
            address_spaces: BTreeMap::new(),
            placement: Vec::new(),
            swizzles: Vec::new(),
            contexts: BTreeMap::new(),
            alignment,
        }
    }
}

///// Generate 4-level page tables from this GLAS for this context.
/////
///// TODO: this should probably be a function per-mmu
//pub fn page_table_from_glas(
//    glas: &GLAS,
//    f: BTreeMap<String, RangeTree<Option<(String, Range)>>>,
//    context: usize,
//    base_addr: u128,
//    resources: &Vec<usize>,
//) -> PageTable {
//    let mut page_table = PageTable::new(base_addr);
//
//    let space = &f.r[&context];
//    let mut placed = vec![false; glas.placement.len()];
//    for (ctx_range, resource, resource_range) in
//        space.iter().filter(|(_, r, _)| resources.contains(r))
//    {
//        let i = glas
//            .placement
//            .iter()
//            .position(|(r, _, c)| r == resource && c == resource_range)
//            .unwrap();
//        let (_, glas_range, _) = glas.placement[i];
//
//        if placed[i] {
//            continue;
//        }
//
//        assert!(ctx_range.offset() % 4096 == 0);
//        assert!(glas_range.offset() % 4096 == 0);
//
//        assert!(ctx_range.length() % 4096 == 0);
//        assert!(ctx_range.length() == glas_range.length());
//
//        let sz = if ctx_range.offset() % (1 << 30) == 0
//            && glas_range.offset() % (1 << 30) == 0
//            && glas_range.length() % (1 << 30) == 0
//        {
//            1 << 30
//        } else if ctx_range.offset() % (1 << 21) == 0
//            && glas_range.offset() % (1 << 21) == 0
//            && glas_range.length() % (1 << 21) == 0
//        {
//            1 << 21
//        } else {
//            1 << 12
//        };
//
//        placed[i] = true;
//        for i in 0..ctx_range.length() / sz {
//            let nsz = (sz * i) as u64;
//            let src = glas_range.offset() as u64 + nsz;
//            let dst = ctx_range.offset() as u64 + nsz;
//            match sz {
//                1073741824 => page_table.add_1g_mapping(src, dst),
//                2097152 => page_table.add_2m_mapping(src, dst),
//                4096 => page_table.add_4k_mapping(src, dst),
//                _ => unreachable!(),
//            }
//        }
//    }
//
//    page_table
//}

//#[test]
//fn test_fail_conflict() {
//    let mut dn = DecodingNet::new();
//    let c1 = dn.add_address_space("c1", Range::new(0, 100)).unwrap();
//    let c2 = dn.add_address_space("c2", Range::new(0, 100)).unwrap();
//    let i = dn.add_address_space("i", Range::new(0, 100)).unwrap();
//    let r1 = dn.add_address_space("r1", Range::new(0, 10)).unwrap();
//    let r2 = dn.add_address_space("r2", Range::new(0, 10)).unwrap();
//
//    dn.add_mapping(c1, i, 0, 10, 10).unwrap();
//    dn.add_mapping(c1, i, 10, 0, 10).unwrap();
//    dn.add_mapping(c2, i, 0, 0, 10).unwrap();
//    dn.add_mapping(c2, i, 10, 10, 10).unwrap();
//    dn.add_mapping(i, r1, 0, 0, 10).unwrap();
//    dn.add_mapping(i, r2, 10, 0, 10).unwrap();
//
//    let glas = derive_glas(&dn, &vec![c1, c2], &vec![r1, r2], &Vec::new())
//        .expect("can derive glas");
//    _ = glas;
//}
//
//#[test]
//fn test_flatten() {
//    let mut dn = DecodingNet::new();
//    let c1 = dn.add_address_space("", Range::new(0, 0x100)).unwrap();
//    let c2 = dn.add_address_space("", Range::new(0, 0x100)).unwrap();
//    let c3 = dn.add_address_space("", Range::new(0, 0x100)).unwrap();
//
//    let i = dn.add_address_space("", Range::new(0, 0x100)).unwrap();
//
//    let r1 = dn.add_address_space("", Range::new(0, 0x10)).unwrap();
//    let r2 = dn.add_address_space("", Range::new(0, 0x10)).unwrap();
//    let r3 = dn.add_address_space("", Range::new(0, 0x10)).unwrap();
//
//    dn.add_mapping(c1, i, 0x20, 0x20, 0x50).unwrap();
//    dn.add_mapping(c2, i, 0x20, 0x20, 0x50).unwrap();
//    dn.add_mapping(c3, i, 0x10, 0x10, 0x10).unwrap();
//
//    dn.add_mapping(i, r1, 0x50, 0x0, 0x10).unwrap();
//    dn.add_mapping(i, r2, 0x20, 0x0, 0x10).unwrap();
//    dn.add_mapping(i, r3, 0x10, 0x0, 0x10).unwrap();
//
//    let _ = dn.flatten(&vec![c1, c2, c3]);
//}
