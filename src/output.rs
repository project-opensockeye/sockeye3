const BYTE_PREFIXES: [&str; 7] = ["", "ki", "Mi", "Gi", "Ti", "Pi", "Ei"];

pub fn human_bytes(n: u128) -> String {
    let mut nf = n as f64;
    let mut i = 0;

    while nf >= 1024.0 && i + 1 < BYTE_PREFIXES.len() {
        nf /= 1024.0;
        i += 1;
    }

    format!("{:.2} {}B", nf, BYTE_PREFIXES[i])
}
