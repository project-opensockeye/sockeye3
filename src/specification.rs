use std::collections::BTreeMap;
use std::fmt::Debug;
use std::path::Path;

use crate::decoding_net::*;
use crate::module;
use crate::parser::{self, *};
use crate::range::Range;

use miette::{miette, LabeledSpan, NamedSource, Result};

pub trait Module: Debug {
    fn new_instance(&self) -> Result<Instance>;
}

/// A port is a named range of an address space.
#[derive(Clone, Debug)]
pub struct Port {
    pub name: String,
    pub range: Range,
}

impl Port {
    pub fn new(name: String, range: Range) -> Port {
        Port { name, range }
    }
}

#[derive(Debug)]
pub struct Instance {
    pub module_name: String,
    pub inports: BTreeMap<String, Port>,
    pub outports: BTreeMap<String, Port>,

    pub decoding_net: DecodingNet,
}

#[derive(Debug)]
#[allow(dead_code)]
pub struct Specification {
    // A specification has a name, derived from the file path.
    name: String,

    // The file path to the root file of this specification.
    path: String,

    // A list of modules that were encountered.
    modules: BTreeMap<String, Box<dyn Module>>,
    pub instances: BTreeMap<String, Instance>,
    connections: BTreeMap<String, String>,
    inports: BTreeMap<String, Port>,
    outports: BTreeMap<String, Port>,

    pub decoding_net: DecodingNet,
    pub glas_constraints: Vec<Located<GlasExpression>>,
}

impl Specification {
    pub fn new() -> Specification {
        Specification {
            name: "empty".to_string(),
            path: "".to_string(),

            modules: BTreeMap::new(),
            instances: BTreeMap::new(),
            connections: BTreeMap::new(),
            inports: BTreeMap::new(),
            outports: BTreeMap::new(),

            decoding_net: DecodingNet::new(),
            glas_constraints: Vec::new(),
        }
    }

    pub fn parse(path: &Path) -> miette::Result<Specification> {
        let name = path.file_stem().unwrap().to_string_lossy().to_string();
        let named_source = NamedSource::new(
            path.to_string_lossy(),
            std::fs::read_to_string(path).unwrap(),
        );
        let exprs = parser::parse_soc(path)
            .map_err(|err| err.with_source_code(named_source.clone()))?;

        let mut spec = Specification::new();
        spec.name = name;
        spec.path = path.to_string_lossy().to_string();

        let cur_dir = path.parent().unwrap_or(Path::new("."));
        for expr in exprs {
            let res = match expr.inner {
                SoCExpression::Import(module_path, _doc_string) => {
                    spec.import_module(cur_dir, Path::new(&module_path))
                }
                SoCExpression::Instantiate(
                    inst_name,
                    mod_name,
                    _doc_string,
                ) => {
                    spec.instantiate_module(mod_name, inst_name, expr.location)
                }
                SoCExpression::Connect(
                    out_port,
                    out_range,
                    in_port,
                    in_range,
                    _doc_string,
                ) => spec.connect(
                    &out_port,
                    out_range,
                    &in_port,
                    in_range,
                    expr.location,
                ),
            };

            res.map_err(|err| {
                err.with_source_code(named_source.clone()).wrap_err(format!(
                    "could not parse {}",
                    path.to_string_lossy()
                ))
            })?;
        }

        Ok(spec)
    }

    pub fn import_module(
        &mut self,
        cur_dir: &Path,
        module_path: &Path,
    ) -> Result<()> {
        let path = cur_dir.join(module_path);

        let m = module::Module::parse(&path)?;
        self.register_module(m.get_name().to_string(), Box::new(m));

        Ok(())
    }

    pub fn register_module(&mut self, name: String, module: Box<dyn Module>) {
        self.modules.insert(name.clone(), module);
    }

    pub fn instantiate_module(
        &mut self,
        mod_name: String,
        inst_name: String,
        location: Location,
    ) -> Result<()> {
        let m = match self.modules.get(&mod_name) {
            None => {
                return Err(miette!(
                    code = "spec::module::instantiation",
                    labels = vec![LabeledSpan::at(location.span, "here")],
                    "module {mod_name} not found",
                ))
            }
            Some(m) => m,
        };

        let instance = m.new_instance()?;

        self.decoding_net.add_prefixed_subnet(
            &inst_name,
            &instance.decoding_net,
            location,
        )?;
        self.inports.extend(instance.inports.iter().map(|(k, v)| {
            (
                format!("{inst_name}.{k}"),
                Port {
                    name: format!("{inst_name}.{}", v.name),
                    range: v.range,
                },
            )
        }));
        self.outports.extend(instance.outports.iter().map(|(k, v)| {
            (
                format!("{inst_name}.{k}"),
                Port {
                    name: format!("{inst_name}.{}", v.name),
                    range: v.range,
                },
            )
        }));

        self.instances.insert(inst_name.clone(), instance);

        Ok(())
    }

    pub fn connect(
        &mut self,
        out_port: &str,
        out_range: Option<Range>,
        in_port: &str,
        in_range: Option<Range>,
        loc: Location,
    ) -> Result<()> {
        let sp = self.outports.get(out_port).ok_or(miette!(
            code = "spec::port::doesnotexist",
            "Out port {out_port} does not exist",
        ))?;
        let dp = self.inports.get(in_port).ok_or(miette!(
            code = "spec::port::doesnotexist",
            "In port {in_port} does not exist",
        ))?;

        let out_range = match out_range {
            Some(or) => or,
            None => sp.range,
        };

        let in_range = match in_range {
            Some(ir) => ir,
            None => dp.range,
        };

        if !sp.range.contains(&out_range) {
            return Err(miette!(
                code = "spec::port::out_of_range",
                labels = vec![LabeledSpan::at(loc.span, "here")],
                "requested range {out_range} outside of port range {}",
                sp.range,
            ));
        }

        if !dp.range.contains(&in_range) {
            return Err(miette!(
                code = "spec::port::out_of_range",
                labels = vec![LabeledSpan::at(loc.span, "here")],
                "requested range {out_range} outside of port range {}",
                sp.range,
            ));
        }

        self.decoding_net
            .add_mapping(&sp.name, &dp.name, out_range, in_range, "", loc)?;

        Ok(())
    }
}
