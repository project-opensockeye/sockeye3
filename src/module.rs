use std::collections::BTreeMap;
use std::path::Path;

use crate::decoding_net::*;
use crate::parser::{self, *};
use crate::range::Range;
use crate::specification::{self, *};

use miette::{miette, LabeledSpan, NamedSource, Result};

#[derive(Debug)]
#[allow(dead_code)]
pub struct Module {
    name: String,
    path: String,
    decoding_net: DecodingNet,

    inports: BTreeMap<String, Port>,
    outports: BTreeMap<String, Port>,
}

impl Module {
    pub fn parse(path: &Path) -> Result<Module> {
        let name = path.file_stem();
        let named_source = NamedSource::new(
            path.to_string_lossy(),
            std::fs::read_to_string(path).unwrap(),
        );
        let exprs = parser::parse_module(path)
            .map_err(|err| err.with_source_code(named_source.clone()))?;

        let mut m = Module {
            name: name.unwrap().to_string_lossy().to_string(),
            path: path.to_string_lossy().to_string(),
            decoding_net: DecodingNet::new(),

            inports: BTreeMap::new(),
            outports: BTreeMap::new(),
        };

        for expr in exprs {
            let res = match expr.inner {
                ModuleExpression::AddressSpace(name, sz, attr, desc) => {
                    m.add_address_space(&name, sz, attr, &desc, expr.location)
                }
                ModuleExpression::Mapping(s, d, soff, doff, b, desc) => {
                    m.add_mapping(&s, &d, soff, doff, b, &desc, expr.location)
                }
                ModuleExpression::InPort(p, range, _desc) => {
                    m.add_in_port(&p, range, expr.location)
                }
                ModuleExpression::OutPort(p, range, _desc) => m
                    .add_out_port(&p, range, expr.location.clone())
                    .map_err(|err| {
                        miette!(
                            code = "module::out_port::add",
                            labels = vec![LabeledSpan::at(
                                expr.location.span,
                                "here"
                            )],
                            "cannot add out port: {err}"
                        )
                    }),
            };

            res.map_err(|err| {
                err.with_source_code(named_source.clone())
                    .wrap_err("Failed to parse module")
            })?;
        }

        Ok(m)
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    fn add_address_space(
        &mut self,
        name: &str,
        size_bytes: u128,
        attribute: AddressSpaceAttribute,
        description: &str,
        location: Location,
    ) -> Result<()> {
        self.decoding_net.add_address_space(
            name,
            description,
            Range::new(0, size_bytes),
            attribute,
            location,
        )?;
        Ok(())
    }

    #[allow(clippy::too_many_arguments)]
    fn add_mapping(
        &mut self,
        src: &str,
        dst: &str,
        src_offset: u128,
        dst_offset: u128,
        length_bytes: u128,
        description: &str,
        location: Location,
    ) -> Result<()> {
        self.decoding_net
            .add_mapping(
                src,
                dst,
                Range::new(src_offset, length_bytes),
                Range::new(dst_offset, length_bytes),
                description,
                location.clone(),
            )
            .map_err(|err| {
                miette!(
                    code = "module::mapping::add",
                    labels = vec![LabeledSpan::at(location.span, "here")],
                    "cannot add mapping: {err}"
                )
            })
    }

    fn add_in_port(
        &mut self,
        name: &str,
        range: Option<(String, Range)>,
        location: Location,
    ) -> Result<()> {
        let port_name = name;

        let addr_space = range.clone().map(|r| r.0).unwrap_or(name.to_string());
        let address_space_range = self.decoding_net.range_of(&addr_space)?;
        let range = range.map(|r| r.1).unwrap_or(address_space_range);

        if !address_space_range.contains(&range) {
            return Err(miette!(
                code = "module::port::out_of_range",
                help = "requested {range}, actual {address_space_range}",
                labels = vec![LabeledSpan::at(location.span, "here")],
                "port definition out of range"
            ));
        }

        self.inports.insert(
            port_name.to_string(),
            Port::new(addr_space.to_string(), range),
        );
        Ok(())
    }

    fn add_out_port(
        &mut self,
        name: &str,
        range: Option<(String, Range)>,
        location: Location,
    ) -> Result<()> {
        let port_name = name;

        let addr_space = range.clone().map(|r| r.0).unwrap_or(name.to_string());
        let address_space_range = self.decoding_net.range_of(&addr_space)?;
        let range = range.map(|r| r.1).unwrap_or(address_space_range);

        if !address_space_range.contains(&range) {
            return Err(miette!(
                code = "module::port::out_of_range",
                help = "requested {range}, actual {address_space_range}",
                labels = vec![LabeledSpan::at(location.span, "here")],
                "port definition out of range"
            ));
        }

        self.outports.insert(
            port_name.to_string(),
            Port::new(addr_space.to_string(), range),
        );
        Ok(())
    }
}

impl specification::Module for Module {
    fn new_instance(&self) -> Result<Instance> {
        Ok(Instance {
            module_name: self.name.clone(),
            decoding_net: self.decoding_net.clone(),
            inports: self.inports.clone(),
            outports: self.outports.clone(),
        })
    }
}
