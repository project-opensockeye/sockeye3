use serde::{Deserialize, Serialize};

use miette::{miette, Result};

/// A range represents a fixed-length slice, consisting of some initial offset
/// plus a length of data.
///
/// Ranges can be empty.
#[derive(
    Debug, PartialEq, Eq, Clone, Copy, Serialize, Deserialize, PartialOrd, Ord,
)]
pub struct Range {
    start: u128,
    length: u128,
}

pub type RangeSet = RangeTree<()>;

impl std::fmt::Display for Range {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:#x}..{:#x}", self.start, self.last())
    }
}

impl Range {
    pub fn new(start: u128, length: u128) -> Range {
        Range { start, length }
    }

    pub const fn offset(self) -> u128 {
        self.start
    }

    pub const fn start(self) -> u128 {
        self.start
    }

    /// The (exclusive) last address covered by this range.
    pub const fn end(self) -> u128 {
        self.start + self.length
    }

    pub const fn length(self) -> u128 {
        self.length
    }

    pub const fn last(self) -> u128 {
        self.end() - 1
    }

    pub fn join(self, other: Range) -> Range {
        assert!(self.end() == other.start());
        Range::new(self.offset(), self.length + other.length)
    }

    pub const fn contains_address(self, addr: u128) -> bool {
        self.start <= addr && addr < self.end()
    }

    pub fn contains(self, other: &Range) -> bool {
        self.start <= other.start && other.end() <= self.end()
    }

    pub fn disjoint(self, other: &Range) -> bool {
        self.end() <= other.start || other.end() <= self.start
    }

    pub fn overlaps(self, other: &Range) -> bool {
        !self.disjoint(other)
    }
}

impl Splittable for Range {
    fn split(&mut self, len1: u128) -> Range {
        let old_length = self.length;
        self.length = len1;

        Range::new(self.offset() + len1, old_length - len1)
    }
}

pub trait Splittable {
    fn split(&mut self, len: u128) -> Self;
}

impl Splittable for () {
    #[allow(clippy::unused_unit)]
    fn split(&mut self, _: u128) -> Self {
        ()
    }
}

/// A range tree is an efficient representation of an address space, where
/// non-overlapping ranges are optionally associated with some data.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RangeTree<V> {
    /// The whole range that this range tree covers.
    pub range: Range,

    /// A list of valid offsets, terminated by a one-past-the-end address
    pub offsets: Vec<u128>,

    /// A collection of ranges, indexed by offset.
    pub ranges: Vec<Range>,

    /// An offset-indexed tree holding information about the mapped entries.
    pub values: Vec<Vec<V>>,
}

impl<V> RangeTree<V>
where
    V: Splittable,
{
    /// Create a new range tree at this address with an empty covered range.
    ///
    /// This tree is the left and right identity for tree concatenation, i.e. it
    /// is guaranteed that a.concat(RangeTree::null) == a.
    pub fn null(addr: u128) -> RangeTree<V> {
        RangeTree {
            range: Range::new(addr, 0),
            offsets: vec![addr],
            ranges: Vec::new(),
            values: Vec::new(),
        }
    }

    /// Constructs a new range tree.
    pub fn new(range: Range) -> RangeTree<V> {
        RangeTree {
            range,
            offsets: vec![range.start, range.end()],
            ranges: vec![range],
            values: vec![Vec::new()],
        }
    }

    /// Assign a value to the given range range.
    pub fn assign(&mut self, range: Range, mut v: V) {
        self.split_at(range.start);
        self.split_at(range.end());

        let start_idx = self.offsets.binary_search(&range.start).unwrap();
        let end_idx = self.offsets.binary_search(&range.end()).unwrap();
        for i in start_idx..end_idx {
            let len = self.offsets[i + 1] - self.offsets[i];
            let next = v.split(len);
            self.values[i].push(v);
            v = next;
        }
    }

    /// Iterate over all the entries.
    pub fn iter(&self) -> impl Iterator<Item = (Range, &Vec<V>)> {
        self.ranges.iter().copied().zip(self.values.iter())
    }

    /// Iterate over all the entries.
    pub fn iter_mapped(&self) -> impl Iterator<Item = &V> {
        self.iter().filter_map(|(_, vs)| vs.first())
    }

    /// Merge the assignments of two range trees. Only works if the range trees
    /// use identical ranges.
    pub fn merge(&mut self, other: RangeTree<V>) -> Result<()> {
        if self.range != other.range {
            return Err(miette!(
                help = "cannot merge trees of different ranges, try \"concat\" instead?",
                help = format!("left: {}, right: {}", self.range, other.range),
                "range_tree::merge::invalid_operaiton"
            ));
        }

        for (r, vs) in other {
            for v in vs {
                self.assign(r, v);
            }
        }

        Ok(())
    }

    /// Concatenate these two range trees. Only works if their ranges are
    /// consecutive and non-overlapping, i.e.
    ///
    /// ```text
    /// |     self        |
    ///                   |         other          |
    ///
    ///        will be concatenated to
    ///
    /// |           self.concat(other)             |
    /// ```
    pub fn concat(&mut self, other: RangeTree<V>) -> Result<()> {
        if self.range.end() != other.range.start() {
            return Err(miette!(
                help = "cannot merge non-consecutive trees, try \"merge\" instead?",
                help = format!("left: {}, right: {}", self.range, other.range),
                "range_tree::concat::invalid_operation"
            ));
        }

        self.range = self.range.join(other.range);
        self.offsets.extend(other.offsets.into_iter().skip(1));
        self.ranges.extend(other.ranges);
        self.values.extend(other.values);

        Ok(())
    }

    /// Return an iterator over the ranges of this range tree. The returned
    /// iterator is guaranteed to totally cover the entire requested range.
    ///
    /// For example, if given range is r, then we iterate over ranges s..e
    /// (inclusive).
    ///
    /// ```text
    ///       |        r           |
    /// |    s    |     ..    |    e       |
    /// ```
    pub fn iter_range(
        &self,
        r: Range,
    ) -> impl Iterator<Item = (Range, &Vec<V>)> {
        let start_idx = match self.offsets.binary_search(&r.offset()) {
            Ok(i) => i,
            Err(i) => i - 1,
        };

        let end_idx = match self.offsets.binary_search(&r.end()) {
            Ok(i) => i,
            Err(i) => i,
        };

        self.ranges[start_idx..end_idx]
            .iter()
            .copied()
            .zip(self.values[start_idx..end_idx].iter())
    }

    /// Split this range tree at that address.
    ///
    /// Returns an error if split lies in a range with an associated value.
    pub fn split_at(&mut self, addr: u128) {
        assert!(self.offsets.first() <= Some(&addr));

        let insertion_idx = match self.offsets.binary_search(&addr) {
            Ok(_) => return,
            Err(i) => i,
        };
        assert!(insertion_idx < self.offsets.len());
        assert!(insertion_idx > 0);

        let prev_idx = insertion_idx - 1;
        let prev_range = &mut self.ranges[prev_idx];

        let len1 = addr - prev_range.offset();
        let right = prev_range.split(len1);

        assert!(len1 > 0);

        let mut right_values = Vec::new();
        let prev_values = &mut self.values[prev_idx];
        for v in prev_values.iter_mut() {
            let right = v.split(len1);
            right_values.push(right);
        }

        self.offsets.insert(insertion_idx, addr);
        self.ranges.insert(insertion_idx, right);
        self.values.insert(insertion_idx, right_values);
    }
}

impl<V> IntoIterator for RangeTree<V> {
    type Item = (Range, Vec<V>);
    type IntoIter = ::std::iter::Zip<
        ::std::vec::IntoIter<Range>,
        ::std::vec::IntoIter<Vec<V>>,
    >;

    fn into_iter(self) -> Self::IntoIter {
        self.ranges.into_iter().zip(self.values)
    }
}
