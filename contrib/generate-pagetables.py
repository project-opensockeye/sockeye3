#!/usr/bin/env python3

# This is the "grubby Python script" that generates page tables for a given
# core based on the computed GLAS.

import argparse
import json
import logging
import sys

parser = argparse.ArgumentParser(
                    prog='generate-pagetables.py',
                    description='What the program does',
                    epilog='Text at the bottom of help')

parser.add_argument('glas_file')
parser.add_argument('--context', required=True)
parser.add_argument('--format', choices=['aarch64', 'riscv64'], default='aarch64')
parser.add_argument('--header', required=True)

def run():
    # Initialize the logger and parse arguments
    logging.getLogger().setLevel(logging.INFO)
    args = parser.parse_args()

    # Check whether we actually support this architecture
    if args.format == 'riscv64':
        print('format not supported')
        return

    # Load the GLAS
    with open(args.glas_file, 'r') as f:
        data = json.load(f)

    context = args.context
    if context not in data['per_context_views']:
        print(f'unknown context {context}')
        print('known contexts: ', ', '.join(data['per_context_views'].keys()))
        return

    table = gen_page_table(data, context)
    result = []
    make_aarch64_page_table(0x8100_0000, table, result)
    with open(args.header, 'w') as f:
        write_c_header(f, 0x8100_0000, result)

def gen_page_table(data, context):
    abstract_page_table = [ None for i in range(512) ]

    s = 0
    addr_space = data['per_context_views'][context]
    for entry in addr_space:
        ctx_begin = entry['begin_phys']
        ctx_end = entry['end_phys']
        glas_begin = entry['begin_glas']
        glas_end = entry['end_glas']

        remaining = entry['size']
        ty = entry['resource_type']

        src = ctx_begin
        dst = glas_begin
        while remaining > 0:
            alignments = [get_page_table_alignment(i) for i in [
                ctx_begin, ctx_end, glas_begin, glas_end,
            ]]
            a = min(alignments)
            lvl = get_page_table_level(a)

            add_entry(abstract_page_table, src, lvl, (dst, ty))

            src += a
            dst += a
            remaining -= a

    return abstract_page_table

def make_aarch64_page_table(base_addr, table, result, entry_offset=0):
    used_pages = 1
    result.extend([0 for i in range(512)])
    for i, entry in enumerate(table):
        if isinstance(entry, list):
            next_addr = base_addr + 4096 * used_pages
            new_offset = entry_offset + 512 * used_pages
            used_pages += make_aarch64_page_table(next_addr, entry, result, new_offset)
            value = next_addr | 0b11

        elif entry is not None:
            # entry is a block entry
            addr, res_type = entry
            if res_type == 'Memory':
                value = addr | 0x701
            else:
                value = addr | 0x707

        else:
            # entry is invalid
            value = 0

        result[entry_offset + i] = value

    return used_pages

def write_c_header(f, base_addr, materialized_table):
    f.write(f'typedef unsigned long long pt_entry;\n')
    f.write(f'unsigned long long num_page_table_entries = {len(materialized_table)};\n')
    f.write(f'pt_entry *page_table_base = (pt_entry *)(0x{base_addr:x});\n')
    f.write(f'pt_entry page_table_entries[{len(materialized_table)}] = {{\n')

    for i, page in enumerate(batch(materialized_table, 512)):
        f.write(f'\t// --------------------- 0x{base_addr + i * 4096:x} --------------------- \n')
        for b in batch(page, 4):
            f.write('\t' + ', '.join([f'0x{v:x}' for v in b]) + ',\n')

    f.write(f'}};\n')

    f.write('''
void activate_page_tables() {
    for (int i = 0; i < num_page_table_entries; i++) {
        page_table_base[i] = page_table_entries[i];
    }
    __asm__ ("LDR     x1, ={:#x}\\n\\t"   // program ttbr0 on this CPU
             "MSR     ttbr0_el1, x1\\n\\t"
             "LDR     x1, =0xff\\n\\t"         // program mair on this CPU
             "MSR     mair_el1, x1\\n\\t"
             "LDR     x1, =0x500803510\\n\\t"  // program tcr on this CPU
             "MSR     tcr_el1, x1\\n\\t"
             "ISB\\n\\t"
             "MRS     x2, tcr_el1\\n\\t"       // verify CPU supports desired config
             "CMP     x2, x1\\n\\t"
             "B.NE    .\\n\\t"
             "LDR     x1, =0x1005\\n\\t"       // program sctlr on this CPU
             "MSR     sctlr_el1, x1\\n\\t"
             "ISB\\n\\t"                       // synchronize context on this CPU
             : : :
    );
}
''')

def add_entry(table, addr, level, value, depth=0):
    index = (addr >> 12 + 9 * (3 - depth)) % 512
    if depth == level:
        table[index] = value
    else:
        if table[index] is None:
            table[index] = [ None for i in range(512) ]

        add_entry(table[index], addr, level, value, depth=depth+1)

def get_page_table_level(addr):
    if addr % (1 << 30) == 0:
        return 1
    elif addr % (1 << 21) == 0:
        return 2
    elif addr % (1 << 12) == 0:
        return 3
    else:
        raise 'Error! less-than-page-alignment detected'

def get_page_table_alignment(addr):
    if addr % (1 << 30) == 0:
        return 1 << 30
    elif addr % (1 << 21) == 0:
        return 1 << 21
    elif addr % (1 << 12) == 0:
        return 1 << 12
    else:
        raise 'Error! less-than-page-alignment detected'

def batch(iterable, n=1):
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx+n, l)]

if __name__ == '__main__':
    run()
