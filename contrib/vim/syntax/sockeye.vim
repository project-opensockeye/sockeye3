syntax keyword sockeyeKeyword as in out ctx map mem dev res conn inst import mod soc
syntax keyword sockeyeKeyword in_size out_size
syntax keyword sockeyeKeyword type in_type out_type
syntax keyword sockeyeTodo TODO FIXME XXX TODO: FIXME: XXX: containedin=sockeyeComment

syntax match sockeyeOperator /\V->/
syntax match sockeyeOperator /\V1>/
syntax match sockeyeOperator /\V*>/
syntax match sockeyeOperator /\V1-1>/
syntax match sockeyeOperator /\V*-*>/

syntax match sockeyeIdentifier /[a-zA-Z][a-zA-Z_.0-9]*/
syntax match sockeyeNumber /\v[0-9][0-9_]* ?(EiB|PiB|TiB|GiB|MiB|kiB|B)?/
syntax match sockeyeHexNumber /\v0x[0-9a-fA-F][_0-9a-fA-F]* ?(EiB|PiB|TiB|GiB|MiB|kiB|B)?/

syntax match sockeyeDocComment / *\/\/\/$/
syntax match sockeyeDocComment / *\/\/\/[^/].*$/
syntax match sockeyeComment / *\/\/$/
syntax match sockeyeComment / *\/\/[^/].*$/
syntax match sockeyeComment / *\/\/\/\/[^/].*$/
syntax match sockeyeComment / *\/\/\/\/$/

highlight link sockeyeKeyword Keyword
highlight link sockeyeTodo Todo

highlight link sockeyeIdentifier Identifier
highlight link sockeyeOperator Operator
highlight link sockeyeNumber Number
highlight link sockeyeHexNumber Number

highlight link sockeyeComment Comment
highlight link sockeyeDocComment Debug

set comments^=:///
set formatoptions+=ro
