# Glassery

The Glassery is a Rust program that parses Sockeye3 specification and runs
various computations on top of it.

For more information see the documentation in the [Kirsch
Handbook](https://sockeye.ethz.ch/kirsch/docs).
